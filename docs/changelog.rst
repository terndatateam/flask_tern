
ChangeLog
=========

Version 0.15.0
--------------

:Date: 2025-02-19

* Full mypy testing
* Update elasticsearch client library to 8.x
* Update Flake rules
* add common gunicorn config to be used like ``--config=python:flask_tern.gunicorn``
* openapi response validation is now optional.
* updated default flask_session storage folder to `flask_session_store`. It interferes with isort
  pkg import resolver.

Breaking changes:
~~~~~~~~~~~~~~~~~

Updated elasticsearch client library to 8.x. It uses keyword only calling parameters and
some parameters like ``body=`` have been deprecated / are no longer supported.
Also request timeout is now configured globally on Elasticsearch client object and no longer
available on api methods.

OpenApi response validation can now be enabled / disabled. ``flask_tern.openapi.decorators.openapi`` decorator setting takes
precedence. If the app runs in test or debug then response validation is active. Otherwise
the new config `flask_tern.openapi.settings.OPENAPI_RESPONSE_VALIDATION` set's default behaviour (which is ``False``)


Version 0.14.0
--------------

:Date: 2025-02-13

* Upadated to Flask 3.1.0.
* Added support for validating async view functions in ``flask_tern.openapi.decorators``.
* Introduced a custom ``TernFlaskOpenAPIRequest`` that subclasses ``openapi_core.contrib.flask.handlers.FlaskOpenAPIRequest``
  and overrides ``get_path`` to respect external sub path hosting.


Version 0.13.0
--------------

:Date: 2024-05-09

* compatibility with python 3.12
* Updated openapi decorator to use the callable :py:meth:`openapi_core.contrib.flask.handlers.FlaskOpenAPIErrorsHandler.__call__`
  instead of the removed static method :py:meth:`openapi_core.contrib.flask.handlers.FlaskOpenAPIErrorsHandler.handle`.
* Updated openapi-core to < 0.20.0 (and its other openapi dependencies), >0.19.0 needed for python 3.12 compatibility.
* SESSION_TYPE=filesystem implementation and it's configuration settings are now part of this package (using flask_session cachelib backend). It has been deprecated in flask_session.


Version 0.12.0
--------------

:Date: 2023-09-29

* Updated config parser to use :py:meth:`flask:flask.Config.from_env_var` and :py:meth:`flask:flask.Config.from_prefixed_env`.
  This may potentially be a breaking change as the values are parsed as json first before our custom parser is applied.
* Fix and update :py:func:`flask_tern.auth.login.authorize` to handle OAuthError when authorising OIDC token.
* Pass new auth_error cookie with error message from the OAuthError exception to be read in UI if needed.


Version 0.11.2
--------------

:Date: 2023-05-30

* Fix and update :py:func:`flask_tern.healthcheck.add_check` with signatures swapped around but maintain backward
  compatible with deprecation warning.


Version 0.11.1
--------------

:Date: 2023-05-29

* update default swagger UI to latest alpha (openap 3.1)
* add helper method :py:func:`flask_tern.utils.config.add_setting_parser` to register
  custom environment variable converter
* changed pydocstyle to `google` style (use `google-notypes` in VSCode)


Version 0.11.0
--------------

:Date: 2023-05-26

* updated openapi-core to < 0.18.0 ... this may not be backwards compatible


Version 0.10.0
--------------

:Date: 2023-05-21

* make it possible to re-use sqlalchemy models outised of flaskcontext (use ``flask_tern.db.DeclarativeBase`` as baseclass rather than Flask-SQLAlchemy ``db.Model``)
* update Flask<2.4.0 ... behaviour of ``flask.url_for`` has changed. It no longer encodes query params. Behaviour is only defined for route params; don't use it for query params.
* move to pyproject.tml, configure isort, black and flake8
* renamed fixtures ``oidc -> ``_oidc``, ``cache_spec -> _cache_spec``, ``views -> _views``

Version 0.9.2
-------------

:Date: 2023-04-14

* make proxyfix middleware configurable via env vars
* remove support for custom Authorization scheme apikey-v1
* rename api key header to ``X-API-Key``
* set ``ELASTICSEARCH_URL``, ``DOI_URL``, ``APIKEY_SERVICE`` defaults to ``None``
* added ``flask_tern.healthcheck.add_check`` helper method (moved all builtin check methods to sub module)
* add ``flask_tern.init_app`` to configure all bundled extensions in one go


Version 0.9.1
-------------

:Date: 2023-02-03

* fix healthcheck db query (SQLAlchemy: Textual SQL expression 'SELECT 1' should be explicitly declared as text('SELECT 1'))


Version 0.9.0
-------------

:Date: 2023-02-02

* update to Flask 2.2 (some backwards imcompatible changes)
* converted ``flask_tern.utils.json.TernJSONEncoder`` to ``flask_tern.utils.json.TernJSONProvider`` (new JSONProvider API in Flask 2.2)
* converted ``flask_tern.openapi.OpenApiBlueprint`` to a factory method. API is still the same.

Version 0.8.1
-------------

:Date: 2022-04-04

* require Authlib>=1.0.0,<2.0.0

Version 0.8.0
-------------

:Date: 2022-03-10

* update to Flask 2
* removed default db health check (apps need to enable it in app.py)
* scheduled removal of custom auth scheme apikey-v1 - replcaed by custom header X-Api-Key
* support basic auth with username=apikey and password=apikey
  apikey as username with empty password
  apikey as password with empty username
* add generic json error handler to openapi blueprint
* add typeURI and reason field to audit records

Version 0.7.1
-------------

:Date: 2021-21-13

* fix pickling of User objects

Version 0.7.0
-------------

:Date: 2021-11-12

Beaking changes:
~~~~~~~~~~~~~~~~

This release changes the `current_user` object from a `dict` to a custom class. While attempts have been made to stay
API backward compatbile, code using this extension should be updated to support the new `User` object.

* current_user is now an object, making all OIDC claims available
* basic_auth fixture returns list of configured users, and an additional key "auth" which makes it easier to do basic_auth

Version 0.6.5
-------------

:Date: 2021-08-30

* apikeys can be used as username with basic auth (password needs to be empty)

Version 0.6.4
-------------

:Date: 2021-08-30

* accept apikeys via http basic auth

Version 0.6.3
-------------

:Date: 2021-06-16

* pin openapi-core to <0.14 ... openapi-core 0.14 introduced a bug via it's new pathlib dependecy see https://github.com/p1c2u/openapi-core/releases/tag/0.14.0


Version 0.6.2
-------------

:Date: 2021-06-14

* update require_login to redirect to login page if clients explicitly acceps html / xhtml (Accept header check)


Version 0.6.1
-------------

:Date: 2021-05-05

* made logging setup compatible with wsgi servers.


Version 0.6.0
-------------

:Date: 2021-04-29

* removed ``flask_tern.logging.setup_logging`` in favor of ``flask_tern.logging.init_app``
* made it easier to configure logging
* make auth decorators importable directly on ``flask_tern.auth``
* removed backwards compatible ``flask.tern.settings.load_settings`` import
* added ``flask_tern.utils.json.TernJSONEncoder`` ... a json encoder to format date, time, and datetime as ISO8601 strings


Version 0.5.2
-------------

:Date: 2021-04-23

* | moved import ``flask_tern.settings.load_settings`` to ``flask_tern.utils.config.load_settings``.
  | -> backwards compatible import in place, which will be removed in 0.6.0.

Version 0.5.1
-------------

:Date: 2021-04-14

* initial project creation
