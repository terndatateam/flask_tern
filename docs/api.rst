
===
API
===


Default Settings
----------------

.. automodule:: flask_tern.settings
    :members:


Configuration
-------------

.. automodule:: flask_tern.utils.config
    :members: load_settings, as_bool, add_setting_parser, convert_settings


Extensions:
-----------

.. automodule:: flask_tern
    :members:


SQLAlchemy
----------

.. automodule:: flask_tern.db
    :members:


Elasticsearch
-------------

.. automodule:: flask_tern.elasticsearch
    :members:

.. automodule:: flask_tern.elasticsearch.settings
    :members:


Caching
-------

.. automodule:: flask_tern.cache
    :members:


Authentication
--------------


.. automodule:: flask_tern.auth
    :members:

.. automodule:: flask_tern.auth.user
    :members:

.. automodule:: flask_tern.auth.apikey
    :members:

.. automodule:: flask_tern.auth.login
    :members:

.. automodule:: flask_tern.auth.oidc
    :members: get_userinfo_from_accesstoken

.. automodule:: flask_tern.auth.utils
    :members: get_authorization

.. automodule:: flask_tern.auth.settings
    :members:


Logging
-------

.. automodule:: flask_tern.logging
    :members:


OpenAPI
-------

.. automodule:: flask_tern.openapi
    :members:
    :imported-members:

.. automodule:: flask_tern.openapi.settings
    :members:


ProxyFix
--------

.. automodule:: flask_tern.proxyfix
    :members:

.. automodule:: flask_tern.proxyfix.settings
    :members:


Testing
-------

.. automodule:: flask_tern.testing.fixtures
    :members:
