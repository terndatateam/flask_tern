
=======
Logging
=======

The `Logging` extension uses `python-json-logger <https://pypi.org/project/python-json-logger/>`_ to format all log messages as json.
The default setup tries to configure all loggers (even those from installed libraries) to write logs to stdout.

If the Flask app runs in development mode, log messages are written as plain text to the console.

Additionally this module configures a new log level named `AUDIT`. This log level has highest possible priority to make sure that audit logs
are always produced.

Use the modules :py:func:`flask_tern.logging.init_app` method to configure your app logging.

.. code:: python

    from flask import Flask
    from flask_tern import app_logging

    app = Flask(__name__)
    # configure loggers as early as possible
    app_logging.init_app()


From here on python logging has been set up, and normal python logging can be used as usual.
Please make sure to not configure loggers on module level (import time), as this may interfer with logging setup.

Ideally ``__name__`` is the top level package name of the application. This way the Flask app logger becomes the "root" logger 
for the whole package and only one logger needs to be configured to set the log level for the whole application.
This setup will re-use the default flask logging handler, which writes to the wsgi servers ``wsgi.errors`` stream. 
This will make sure, that logs end up at the WSGI servers configured log destination. See :doc:`flask:logging`.


Customise Logging
-----------------

The library supports various configuration options to customise logging.

- LOG_LEVEL: set root logger log level. Values can be strings or constants as described in :ref:`python:levels`.


Audit Logging
-------------

Audit logging follows the Cloud Aditing Data Federation standard `CADF <https://www.dmtf.org/standards/cadf>`_.
CADF specifies a data model to capture Initiator, Action, Target and Observer of events.
A CADF message looks like this:

.. code:: json

    {
        "action": "<action name>",
        "outcome": "success | failure",
        "observer": {
            "id": "<app id / uri>",
            "addresses": {
                "name": "<app name>",
                "url": "<app url"
            }
        },
        "initiator": {
            "id": "<user id>",
            "credentials": {
                "type": "<auth method>"
            }
            "host": {
                "agent": "<user agent>",
                "ip": "<source ip>"
            }
        },
        "target": {
            "id": "<target id>",
            "<extras>": "<extra infos about target>",
            "addresses": [
                {
                    "name": "<app name>",
                    "url": "<requset url>"
                }
            ]
        }
    }


Audit messages will be logged with logger name `AUDIT` and severity `AUDIT` for easy log parsing and audit capturing.

The module provides a few helper methods to make capturing audit messages easier.
Most fields are filled out based on information from the current request, but information about the target needs to be added in the application.

.. py:currentmodule:: flask_tern.logging

.. autodecorator:: audit_log
    :noindex:

.. autofunction:: record_audit_target
    :noindex:

.. autofunction:: audit_ignore
    :noindex:

.. autofunction:: audit_success
    :noindex:

.. autofunction:: log_audit
    :noindex:


.. TODO::

    - define vocabularies for audit log objects
    - make sure json log fields are easy to handle in ES
