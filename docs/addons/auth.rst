
==============
Authentication
==============

This module provides a simple setup and helper methods to authenticate users either based on
HTTP Authorization header, query parameter or a cookie based session.

This addon can be included like this:

.. code:: python

    from flask import Flask
    from flask_tern import auth

    app = Flask(__name__)

    # here is a good place to set app.config values if needed.

    auth.init_app(app)


:py:func:`flask_tern.auth.init_app` configures :py:class:`authlib's flask integration <authlib:authlib.integrations.flask_client.OAuth>`.
The authlib extension instance is available anytime as global object importable as::

    from flask_tern.auth import oauth

    # The default client is registered as "oidc"
    oauth.oidc

This instance allows access to current valid tokens and other token related methods.


How it works
============

Main entry point to user authentication is the local proxy :py:data:`flask_tern.auth.current_user`. This proxy defers authentication attemps to
:py:func:`flask_tern.auth.utils.get_authorization`, which first looks for a HTTP Authorization header, which it decodes. If the header is available
it will be parsed and authentication type (e.g. basic, bearer, etc...), and data will be extracted.
If this header is not set, it will look for a `X-Api-Key` header.
As a last attempt it will check whether there is a session (cookie) with valid login data.

Header formats are as usual for `basic` aund `bearer` method. The custom `apikey-v1` authorization scheme ose not use base64 encoding.
The accepted value for the `X-Api-Key` is the same as for `apikey-v1` auth scheme.

Based on authentication method, the app will look into :py:data:`USERINFO_MAP` to parse and validate authentication data.
If all goes well, `current_user` will be a dictionary with authenticated and validated user information.

The whole authentication process also sets some request global variables in :py:data:`flask:flask.g`:

    - **g.current_user**: a :py:class:`flask_tern.auth.user.User` object with information about the current authenticatet user or None
    - **g.auth_type**: the authentication type `basic`, `bearer`, `apikey-v1`, `session`
    - **g.auth_info**: the full authentication as extracted by :py:func:`flask_tern.auth.utils.get_authorization`

This whole auth process is triggered once per request by accessing :py:data:`flask_tern.auth.current_user`.


View Decorators
===============

The following is a list of view decorators provided by :py:mod:`flask_tern.auth`. These decorators can be used to enforce authentication for view methods.
All view decorators access :py:data:`flask_tern.auth.current_user` which kicks off the authentication process.

.. autofunction:: flask_tern.auth.require_user
    :noindex:

.. autofunction:: flask_tern.auth.require_login
    :noindex:

.. autofunction:: flask_tern.auth.require_roles
    :noindex:


Auth Settings
=============

Authentication setup requires a few configuration settings.


General
-------

.. autodata:: flask_tern.auth.settings.USERINFO_MAP
    :noindex:
    :annotation:

This dictionary is used to extract user details from request depending on authentication method.

.. list-table:: USERINFO_MAP default values
    :widths: 25 70
    :header-rows: 1

    * - Auth method
      - default extractor
    * - `bearer`
      - :py:func:`flask_tern.auth.oidc.get_userinfo_from_accesstoken`
    * - `session`
      - :py:func:`flask_tern.auth.login.get_userinfo_from_session`
    * - `apikey-v1`
      - :py:func:`flask_tern.auth.apikey.get_userinfo_from_apikey`


OpenID Connect
--------------

.. autodata:: flask_tern.auth.settings.OIDC_DISCOVERY_URL
    :noindex:
.. autodata:: flask_tern.auth.settings.OIDC_CLIENT_ID
    :noindex:
.. autodata:: flask_tern.auth.settings.OIDC_CLIENT_SECRET
    :noindex:
.. autodata:: flask_tern.auth.settings.OIDC_SCOPE
    :noindex:
.. autodata:: flask_tern.auth.settings.OIDC_USE_REFRESH_TOKEN
    :noindex:


Api Key Authentication
----------------------

.. autodata:: flask_tern.auth.settings.APIKEY_SERVICE
    :noindex:



Authentication methods
=======================

This section describes the supported authentication methods in detail.

OpenID Connect Session
-----------------------

The module :py:mod:`flask_tern.auth.login` provides a flask Blueprint to support session based login via browsers as well.

.. code:: python

    from flask_tern.auth.login import oidc_login

    app = Flask(__name__)

    app.register_blueprint(oidc_login, url_prefix="/api/oidc")


This setup will configure 3 url endpoints.

    - **/api/oidc/login**: start OIDC login dance
    - **/api/oidc/authorize**: return url from identity provider
    - **/api/oidc/logout**: clears current users session triggering a logout


See the respective view functions:

.. autofunction:: flask_tern.auth.login.login
    :noindex:

.. autofunction:: flask_tern.auth.login.authorize
    :noindex:

.. autofunction:: flask_tern.auth.login.logout
    :noindex:


After successful login, authenticated user info will be stored in the session under key `oidc.user_info`. Best way to retrieve this data is by accessing
:py:data:`flask_tern.auth.current_user`.

If the setting `OIDC_USE_REFRESH_TOKEN` is enable (set to `True`), the application will keep OIDC id-, acces-, and refresh token up to date in the session.
This means that the application code can use these tokens anytime to call out to additional services accepting these tokens.
If token refresh is enabled, all current tokens are stored under `oidc.tokens` in the session, from where they are picked up by the authlib Flask integration.

If the view :py:func:`flask_tern.auth.login.authorize` fails to authorise the OIDC token it will return a redirect to either previous url, or application root.
Additionally a cookie `auth_error` will be set in the response carrying the error message.


Session handling
^^^^^^^^^^^^^^^^

The application integrates :py:mod:`Flask-Session <session:flask.ext.session>` which offers a configurable session backend for flask applications.
Please see :py:mod:`Flask-Session Configuration <flask.ext.session>` section for details.

Common configuration values:

.. autodata:: flask_tern.settings.SECRET_KEY
    :annotation: = os.urandom(16)
    :noindex:

.. autodata:: flask_tern.settings.SESSION_TYPE
    :annotation: = None
    :noindex:

.. autodata:: flask_tern.settings.SESSION_FILE_DIR
    :noindex:

.. autodata:: flask_tern.settings.SESSION_FILE_THRESHOLD
    :noindex:

.. autodata:: flask_tern.settings.SESSION_FILE_MODE
    :noindex:

.. autodata:: flask_tern.settings.SESSION_REDIS
    :noindex:


OAuth2 Bearer token
-------------------

A request may contain an OAuth2 bearer token in the Authorization header. It assumed that the token provided is a JWT encoded access_token (as provided by keycloak) and user info can be extracted directly from the token.

See :py:func:`flask_tern.auth.oidc.get_oauth_userinfo` or details.


API Key
-------

This method relies on a deployed API Key service. This method asks the API Key service to validate the given token.
The API Key service is expected to return user info and roles, in the same format as Keycloak produces access tokens.

This method utilises :py:mod:`flask_tern.cache` to cache validation results. This is done to improve response time of subsequent request with API Keys.
To enable caching it is necessary to configure :doc:`flaskcache:index` properly.

.. code-block::

    # e.g.: use simple caching
    CACHE_TYPE = "SimpleCache"

See :py:func:`flask_tern.auth.apikey.get_userinfo_from_apikey` for details.