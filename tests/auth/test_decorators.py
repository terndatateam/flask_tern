import pytest
from flask import g
from werkzeug.exceptions import Forbidden

from flask_tern.auth import User
from flask_tern.auth.decorators import require_login, require_roles


def _dummy():
    return "ok"


def test_require_roles_no_user(app):
    method = require_roles(["role1"])(_dummy)
    with app.test_request_context("/"):
        from werkzeug.exceptions import Forbidden

        with pytest.raises(Forbidden):
            method()


def test_require_roles_ok1(app):
    method = require_roles(["role1"])(_dummy)
    with app.test_request_context("/"):
        g.current_user = User(id="", name="", roles=["role1", "role2"])
        assert method() == "ok"


def test_require_roles_ok2(app):
    method = require_roles(["role1", "role2"])(_dummy)
    with app.test_request_context("/"):
        g.current_user = User(id="", name="", roles=["role1", "role2"])
        assert method() == "ok"


def test_require_roles_fail(app):
    method = require_roles(["role1", "role2", "role3"])(_dummy)
    with app.test_request_context("/"):
        g.current_user = User(id="", name="", roles=["role1", "role2"])
        with pytest.raises(Forbidden):
            method()


@pytest.mark.parametrize(
    "headers",
    [
        {"Accept": "text/html"},
        {"Accept": "application/xhtml+html"},
        {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        },
    ],
)
@pytest.mark.usefixtures("_oidc")
def test_require_login(app, headers):
    method = require_login(_dummy)
    with app.test_request_context("/", headers=headers):
        response = method()
        assert response.status_code == 302
        assert (
            response.headers["location"]
            == "http://localhost/api/oidc/login?return_url=http%3A%2F%2Flocalhost%2F"
        )


@pytest.mark.parametrize(
    "headers",
    [
        {"Accept": "application/json"},
        {"Accept": "*/*"},
        {
            "Accept": "application/xml;q=0.9,image/webp,*/*;q=0.8",
        },
    ],
)
@pytest.mark.usefixtures("_oidc")
def test_require_login_fail(app, headers):
    method = require_login(_dummy)
    with app.test_request_context("/", headers=headers), pytest.raises(Forbidden):
        method()


@pytest.mark.usefixtures("_oidc")
def test_require_login_params(app):
    method = require_login(_dummy)
    with app.test_request_context("/?a=b", headers={"Accept": "text/html"}):
        response = method()
        assert response.status_code == 302
        assert (
            response.headers["location"]
            == "http://localhost/api/oidc/login?return_url=http%3A%2F%2Flocalhost%2F%3Fa%3Db"
        )


def test_require_login_ok(app):
    method = require_login(_dummy)
    with app.test_request_context("/"):
        g.current_user = {"name": "Test"}
        response = method()
        assert response == "ok"
