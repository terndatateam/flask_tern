from urllib.parse import parse_qsl, urlparse

import pytest
import requests


@pytest.mark.usefixtures("_oidc", "mock_kc")
def test_oauth_login(client, views):
    # test full oauth login dance
    response = client.get("/api/oidc/login")
    # expect a redirect
    assert response.status_code == 302
    location = response.headers["Location"]
    # there should be a state parameter in authorize redirect
    assert "state=" in location
    # remember state for later
    state = dict(parse_qsl(urlparse(location).query))["state"]
    assert state is not None
    # simulate authorize callback to keycloak (we need to pass nonce )
    response = requests.get(location, allow_redirects=False, timeout=600)
    # session cookie from previous request should still be available
    response = client.get(response.headers["location"])
    # expect redirect on success
    assert response.status_code == 302
    assert response.headers["Location"] == "http://localhost/"

    # re-use session from above to get authorised response?
    response = client.get("/private")
    assert response.status_code == 200
    assert response.json["id"] == "123"


@pytest.mark.usefixtures("_oidc")
def test_oauth_bearer(client, views, mock_kc):
    response = client.get(
        "/private",
        headers={"Authorization": "Bearer {}".format(mock_kc["access_token"])},
    )
    assert response.status_code == 200
    assert response.json["id"] == "123"


@pytest.mark.usefixtures("_oidc")
def test_oauth_bearer_invalid(client, views, mock_kc):
    response = client.get(
        "/private",
        headers={"Authorization": "Bearer {}".format(mock_kc["expired_token"])},
    )
    assert response.status_code == 403


# TODO: test HTTP Referrer
# TODO: test return_url parameter
# TODO: test invalid token parser (audit login failed)
# TODO: test OIDC_USE_REFRESH_TOKEN
# TODO: test logout (_save_return_url is a bit weird?)
