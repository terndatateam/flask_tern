from unittest.mock import patch

import pytest

from flask_tern.auth.user import User


@pytest.mark.usefixtures("_oidc", "mock_kc")
def test_authorize_fail(client, views):
    """Simulate OIDC login error."""
    query_string = {"error": "access_denied", "state": "somestate"}
    response = client.get("/api/oidc/authorize", query_string=query_string)
    cookies = response.headers.getlist("Set-Cookie")

    assert "auth_error" in cookies[0]
    assert response.status_code == 302


@pytest.mark.usefixtures("_oidc", "mock_kc", "_oidc")
def test_authorize_token(client, views, mock_kc):
    """Simulate successful login error."""
    query_string = {
        "code": "somecode",
        "state": "somestate",
    }

    with patch(
        "flask_tern.auth.login.oauth.oidc.authorize_access_token"
    ) as mock_authorize_access_token:
        mock_authorize_access_token.return_value = {
            "access_token": mock_kc["access_token"],
            "expires_in": 3600,
        }

        with patch("flask_tern.auth.login.get_oauth_userinfo") as mock_get_oauth_userinfo:
            mock_get_oauth_userinfo.return_value = User(
                id="12", name="User1", email="user@email.com"
            )

            with client:
                response = client.get(
                    "/api/oidc/authorize",
                    query_string=query_string,
                )

                assert response.status_code == 302
                cookies = response.headers.getlist("Set-Cookie")
                assert "auth_error" not in cookies[0]


@pytest.mark.usefixtures("_oidc", "mock_kc")
def test_authorize_no_user(client, views, mock_kc):
    """Simulate OIDC login error."""
    # Construct the authorization URL with the valid state parameter.

    query_string = {
        "code": "somecode",
        "state": "somestate",
    }

    with patch(
        "flask_tern.auth.login.oauth.oidc.authorize_access_token"
    ) as mock_authorize_access_token:
        # patch function to return response
        mock_authorize_access_token.return_value = {
            "access_token": mock_kc["access_token"],
            "expires_in": 3600,
        }

        with patch("flask_tern.auth.login.get_oauth_userinfo") as mock_get_oauth_userinfo:
            mock_get_oauth_userinfo.return_value = None

            with client:
                response = client.get("/api/oidc/authorize", query_string=query_string)

                # returns redirect response
                assert response.status_code == 302

                # check session is cleared                assert len(session) == 0
