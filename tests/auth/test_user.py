import pickle  # noqa: S403


def test_pickle():
    from flask_tern.auth.user import User

    user = User("id", "name")
    user2 = pickle.loads(pickle.dumps(user))  # noqa: DUO103,S301
    assert user.id == user2.id
    assert user2.name == user2.name
