import base64


def test_basic_auth_ok(client, basic_auth, views):
    response = client.get(
        "/private",
        headers={"Authorization": basic_auth["user"].auth},
    )
    assert response.status_code == 200
    assert response.json["id"] == "user"


def test_basic_auth_invalid(client, views):
    response = client.get(
        "/private",
        headers={"Authorization": "Basic {}".format(base64.b64encode(b"test").decode("ascii"))},
    )
    assert response.status_code == 403
