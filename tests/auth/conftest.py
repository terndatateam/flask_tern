import pytest


@pytest.fixture
def app_config(app_config):
    app_config.update(
        {
            "OIDC_DISCOVERY_URL": "https://auth.example.com/.well-known/openid-configuration",
            "OIDC_CLIENT_ID": "oidc-test",
            "APIKEY_SERVICE": "https://auth.example.com/apikey/api/v1.0",
        }
    )
    return app_config
