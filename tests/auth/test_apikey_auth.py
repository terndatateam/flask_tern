import base64

import pytest


@pytest.mark.parametrize(("apikey", "outcome"), [("Whatever", 200), ("Invalid", 403)])
@pytest.mark.usefixtures("_oidc", "cache", "mock_kc")
def test_apikey_auth_header(apikey, outcome, client, views):
    response = client.get(
        "/private",
        headers={"X-Api-Key": apikey},
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"


# TODO: remove this test .... custom ApiKey-v1 auth scheme no longer supported
@pytest.mark.parametrize(("apikey", "outcome"), [("Whatever", 403), ("Invalid", 403)])
@pytest.mark.usefixtures("_oidc", "cache", "mock_kc")
def test_apikey_auth_custom(apikey, outcome, client, views):
    response = client.get(
        "/private",
        headers={"Authorization": "ApiKey-v1 {}".format(apikey)},
    )
    assert response.status_code == outcome


@pytest.mark.parametrize(("apikey", "outcome"), [(b"Whatever", 200), (b"Invalid", 403)])
@pytest.mark.usefixtures("_oidc", "cache", "mock_kc")
def test_apikey_basic_auth_user(apikey, outcome, client, views):
    response = client.get(
        "/private",
        headers={
            "Authorization": "Basic {}".format(
                base64.b64encode(b"".join((apikey, b":"))).decode("ascii")
            )
        },
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"


@pytest.mark.parametrize(("apikey", "outcome"), [(b"Whatever", 200), (b"Invalid", 403)])
@pytest.mark.usefixtures("_oidc", "cache", "mock_kc")
def test_apikey_basic_auth_pass(apikey, outcome, client, views):
    response = client.get(
        "/private",
        headers={
            "Authorization": "Basic {}".format(
                base64.b64encode(b"".join((b":", apikey))).decode("ascii")
            )
        },
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"


@pytest.mark.parametrize(("apikey", "outcome"), [(b"Whatever", 200), (b"Invalid", 403)])
@pytest.mark.usefixtures("_oidc", "cache", "mock_kc")
def test_apikey_basic_auth_user_pass(apikey, outcome, client, views):
    response = client.get(
        "/private",
        headers={
            "Authorization": "Basic {}".format(
                base64.b64encode(b"".join((b"apikey:", apikey))).decode("ascii")
            )
        },
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"


@pytest.mark.parametrize(("apikey", "outcome"), [("Whatever", 200)])
@pytest.mark.usefixtures("_oidc", "mock_kc")
def test_apikey_auth_cache(apikey, outcome, client, cache, views):
    response = client.get(
        "/private",
        headers={"X-Api-Key": apikey},
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"
    # check cache key
    cache_key = "auth.apikey.{}".format(apikey)
    user = cache.get(cache_key)
    assert user["id"] == "user"
    # do auth again to trigger full code coverage in auth.apikey.py
    response = client.get(
        "/private",
        headers={"X-Api-Key": apikey},
    )
    assert response.status_code == outcome
    if outcome == 200:
        assert response.json["id"] == "user"
