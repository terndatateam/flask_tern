import pytest


@pytest.mark.usefixtures("_oidc")
def test_check_keycloak_fail(app):
    from flask_tern.healthcheck.checks import check_keycloak

    with app.app_context():
        result = check_keycloak()
    # default setup, fails ....
    assert result[0] is False


@pytest.mark.usefixtures("_oidc")
def test_check_keycloak_ok(app, mock_healthchecks):
    from flask_tern.healthcheck.checks import check_keycloak

    with app.app_context():
        result = check_keycloak()
    # default setup, fails ....
    assert result[0] is True


def test_check_doi_fail(app, mock_healthchecks):
    from flask_tern.healthcheck.checks import check_doi

    app.config["DOI_URL"] = app.config["DOI_URL"].replace("/doi.", "/doifail.")
    with app.app_context():
        result = check_doi()
    # default setup, fails ....
    assert result[0] is False


def test_check_doi_ok(app, mock_healthchecks):
    from flask_tern.healthcheck.checks import check_doi

    with app.app_context():
        result = check_doi()
    # default setup, fails ....
    assert result[0] is True
