import json

import pytest
import responses


@pytest.fixture
def mock_healthchecks(app):
    with responses.RequestsMock(assert_all_requests_are_fired=False) as resps:
        doi_url = app.config["DOI_URL"].split("?")[0]
        # keycloak healthcheck just fetches OIDC_DISCOVERY_URL
        resps.add(
            responses.GET,
            app.config["OIDC_DISCOVERY_URL"],
            content_type="application/json",
            status=200,
            body=json.dumps({}),
        )
        # DOI mock response
        resps.add(
            responses.POST,
            # url.startswith("https://doi.example.com/"):
            doi_url,
            content_type="applicatino/xml",
            status=200,
            body=(
                '<?xml version="1.0"?><response type="success">'
                "<responsecode>MT001</responsecode>"
                "<message>DOI status_check was successfully minted.</message>"
                "<doi>status_check</doi><url>status.url.check</url>"
                "<verbosemessage>OK</verbosemessage></response>"
            ),
        )
        # DOI fail test
        resps.add(
            responses.POST,
            # rewrite to fail url
            doi_url.replace("/doi.", "/doifail."),
            status=500,
            body="status check failed",
        )
        yield resps


@pytest.fixture
def app_config(app_config):
    app_config.update(
        {
            # "APIKEY_SERVICE": "https://auth.example.com/apikey/api/v1.0",
            "OIDC_DISCOVERY_URL": "https://auth.example.com/.well-known/openid-configuration",
            "OIDC_CLIENT_ID": "oidc-test",
            "DOI_URL": (
                "https://doi.example.com/test/index.php"
                "?r=api/create&user_id=test-user@example.com&app_id=app-id"
            ),
        }
    )
    return app_config
