import tempfile

from cachelib import FileSystemCache
from flask import Flask

from flask_tern.utils.config import load_settings


def create_app(config=None):
    from flask_tern import logging as app_logging
    from flask_tern.utils.config import load_settings

    app_logging.setup_logging()
    app = Flask(__name__)

    load_settings(app, config=config)
    return app


def test_config(app):
    load_settings(
        app,
        config={
            "TESTING": False,
            "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:",
        },
    )
    assert not app.testing
    load_settings(
        app,
        config={
            "TESTING": True,
            "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:",
        },
    )
    assert app.testing


def test_config_env(app, monkeypatch):
    # mock a config file
    monkeypatch.setenv("FLASK_TERN_SETTINGS", f"{tempfile.gettempdir()}/etc/settings.py")
    config_data = "SOME_CONFIG=1\n" "SQLALCHEMY_DATABASE_URI='sqlite:///:memory:'"
    from unittest.mock import mock_open, patch

    with patch("flask.config.open", mock_open(read_data=config_data)):
        load_settings(app)
        assert app.config["SOME_CONFIG"] == 1


def test_flask_session(app):
    load_settings(
        app,
        config={
            "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:",
            "SESSION_TYPE": "cachelib",
            "SESSION_CACHELIB": FileSystemCache(cache_dir="./flask_session_store"),
        },
    )
    from flask_session import Session

    Session(app)
    import flask_session.base

    assert isinstance(app.session_interface, flask_session.base.ServerSideSessionInterface)


def test_test_config(app):
    assert app.testing
