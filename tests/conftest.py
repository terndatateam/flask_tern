import json
import time
from urllib.parse import parse_qs

import pytest
import responses
from authlib.jose import jwk, jwt
from authlib.oidc.core.grants.util import generate_id_token
from flask import Flask

from flask_tern.testing.fixtures import (  # noqa
    _cache_spec,
    _oidc,
    basic_auth,
    cache,
    db,
    monkeypatch_session,
    views,
)


def create_app(config=None):
    from flask_tern import init_app
    from flask_tern.utils.config import load_settings
    from flask_tern.version import version

    app = Flask(__name__)
    app.config["VERSION"] = version
    load_settings(app, config=config)
    init_app(app)

    return app


@pytest.fixture
def app_config():
    # return a dictionary with app configs
    # this is overridable in subfolders
    return {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:",
        "SQLALCHEMY_ENGINE_OPTIONS": {},
        "PROXYFIX_X_HOST": 1,
        "PROXYFIX_X_PREFIX": 1,
    }


@pytest.fixture
def app(app_config):
    return create_app(config=app_config)


@pytest.fixture
def client(app, basic_auth):  # noqa: F811
    return app.test_client()


def get_bearer_token():
    return {
        "token_type": "Bearer",
        "access_token": "a",
        "refresh_token": "b",
        "expires_in": 3600,
        "expires_at": int(time.time()) + 3600,
    }


def generate_access_token(key, expires_in=3600):
    return jwt.encode(
        {"alg": "HS256", "kid": key["kid"]},
        {
            "iss": "https://auth.example.com",
            "sub": "123",
            "aud": "oidc-test",
            "exp": int(time.time()) + expires_in,
            "iat": int(time.time()),
            "name": "Test User",
            "email": "test_user@example.com",
            "email_verified": True,
            "family_name": "User",
            "given_name": "Test",
            # "roles": {"resource_access": {"client": ["roles", "roles"]}},
            "scope": "openid profile email",
        },
        key,
        # returns a url-safe string (bytes here), so convert it to python string
    ).decode("utf-8")


@pytest.fixture
def mock_kc():
    # used for api key testing?
    key = jwk.JsonWebKey.import_key("secret", {"kty": "oct", "kid": "f"}).as_dict()

    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        # nonce to be returned  for id_token
        state = {}
        # /.well-known/opetid-configuration
        rsps.add(
            responses.GET,
            "https://auth.example.com/.well-known/openid-configuration",
            content_type="application/json",
            status=200,
            body=json.dumps(
                {
                    "issuer": "https://auth.example.com",
                    "authorization_endpoint": "https://auth.example.com/authorize",
                    "token_endpoint": "https://auth.example.com/token",
                    "jwks_uri": "https://auth.example.com/certs",
                    "id_token_signing_alg_values_supported": ["HS256"],
                }
            ),
        )
        # /certs
        rsps.add(
            responses.GET,
            "https://auth.example.com/certs",
            content_type="application/json",
            status=200,
            body=json.dumps({"keys": [key]}),
        )

        # /authorize
        def authorize_callback(request):
            state["nonce"] = request.params["nonce"]
            return (
                302,
                {
                    "location": request.params["redirect_uri"]
                    + "?code=code"
                    + "&state="
                    + request.params["state"]
                },
                None,
            )

        rsps.add_callback(
            responses.GET,
            "https://auth.example.com/authorize",
            content_type="application/json",
            callback=authorize_callback,
        )

        # /token
        def token_callback(request):
            body = parse_qs(request.body)
            # fake token response
            token = get_bearer_token()
            token["access_token"] = generate_access_token(key)
            # grant_type: client_credentials, scope, client_id (oidc-test)
            # grant_type: authorization_code, redirecturi, code, client_id
            if body["grant_type"][0] == "authorization_code":
                # add id_token in case authorization_code grant
                token["id_token"] = generate_id_token(
                    token,
                    {
                        "sub": "123",
                        "name": "Test User",
                        "email": "test_user@example.com",
                        "email_verified": True,
                        "family_name": "User",
                        "given_name": "Test",
                    },
                    key,
                    alg="HS256",
                    iss="https://auth.example.com",
                    aud="oidc-test",
                    exp=3600,
                    nonce=state.get("nonce"),
                )
            return (200, {}, json.dumps(token))

        rsps.add_callback(
            responses.POST,
            "https://auth.example.com/token",
            content_type="application/json",
            callback=token_callback,
        )

        # FIXME: api key response mocks ...
        #        should probably be in it's own fixture (not Keycloak Mock)
        # /api/v1.0/apikeys/validate
        def apikey_callback(request):
            apikey = json.loads(request.body)["apikey"]
            if apikey == "Invalid":
                # response.raise_for_status.side_effect = HTTPError("Client Error",
                #                                                   response=response)
                return (403, {}, "")
            return (
                200,
                {},
                json.dumps(
                    {
                        "sub": "user",
                        "name": "User",
                        "email": "user@example.com",
                        "email_verified": True,
                        "family_name": "Example",
                        "given_name": "User",
                    }
                ),
            )

        rsps.add_callback(
            responses.POST,
            "https://auth.example.com/apikey/api/v1.0/apikeys/validate",
            content_type="application/json",
            callback=apikey_callback,
        )

        # yield rsps
        yield {
            "access_token": generate_access_token(key),
            "expired_token": generate_access_token(key, expires_in=-3600),
        }
