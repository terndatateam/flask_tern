# TODO: this test reconfigures logging, ...
#       it may cause side effects in other test module that rely on specific logging configuration
import logging

from flask import Flask


def create_app(config):
    from flask_tern import logging as app_logging

    app = Flask(__name__)
    app.config.update(config)
    app_logging.init_app(app)
    return app


def test_logging_dev():
    # check log level
    create_app({"DEBUG": True})
    rootLogger = logging.getLogger(__name__)
    assert rootLogger.level == logging.DEBUG


def test_logging_prod():
    create_app({})

    # check log level
    rootLogger = logging.getLogger(__name__)
    assert rootLogger.level == logging.INFO


def test_logging_level():
    create_app({"LOG_LEVEL": "WARNING"})
    logger = logging.getLogger(__name__)
    assert logger.level is logging.WARNING
