import pytest
from flask import abort, g
from werkzeug.exceptions import BadRequest

from flask_tern.auth import User
from flask_tern.logging import audit_log, record_audit_target


def _dummy():
    return "ok"


def _dummy_err(code=None):
    if code:
        abort(code, "Dummy HTTP Error")
    raise Exception("Dummy Error")


def test_audit_decorator(app, caplog, views):
    method = audit_log("action", target="test")(_dummy)

    with app.test_request_context("/"):
        # simulate logged in isuer
        g.current_user = User(id="1", name="Test User")
        g.auth_type = "fake"
        # call our method
        assert method() == "ok"
        # inspect logs
        log_rec = caplog.get_records("call")[0]
        assert log_rec.name == "audit"
        assert log_rec.msg["outcome"] == "success"
        assert log_rec.msg["initiator"]["id"] == "1"


def test_audit_decorator_exc(app, caplog, views):
    method = audit_log("action", target="test")(_dummy_err)

    with app.test_request_context(path="/"):
        # simulate logged in isuer
        g.current_user = User(id="1", name="Test User")
        g.auth_type = "fake"
        # call our method
        with pytest.raises(Exception, match="Dummy Error"):
            method()
        # inspect logs
        log_rec = caplog.get_records("call")[0]
        assert log_rec.name == "audit"
        assert log_rec.msg["outcome"] == "failure"
        assert log_rec.msg["reason"]["reasonCode"] == 500
        assert log_rec.msg["reason"]["message"] == "Dummy Error"
        assert log_rec.msg["initiator"]["id"] == "1"


def test_audit_decorator_http_exc(app, caplog, views):
    method = audit_log("action", target="test")(_dummy_err)

    with app.test_request_context(path="/"):
        # simulate logged in isuer
        g.current_user = User(id="1", name="Test User")
        g.auth_type = "fake"
        # call our method
        with pytest.raises(BadRequest):
            method(400)
        # inspect logs
        log_rec = caplog.get_records("call")[0]
        assert log_rec.name == "audit"
        assert log_rec.msg["outcome"] == "failure"
        assert log_rec.msg["reason"]["reasonCode"] == 400
        assert log_rec.msg["reason"]["message"] == "Dummy HTTP Error"
        assert log_rec.msg["initiator"]["id"] == "1"


def test_audit_record(app, caplog, views):
    method = audit_log("action", target="test")(_dummy)

    with app.test_request_context(path="/"):
        # simulate logged in isuer
        g.current_user = User(id="1", name="Test User")
        g.auth_type = "fake"
        record_audit_target("target_id", extra="target_extra")
        # call our method
        assert method() == "ok"
        # inspect logs
        log_rec = caplog.get_records("call")[0]
        assert log_rec.name == "audit"
        assert log_rec.msg["outcome"] == "success"
        assert log_rec.msg["initiator"]["id"] == "1"
        assert log_rec.msg["target"]["id"] == "target_id"
        assert log_rec.msg["target"]["extra"] == "target_extra"
