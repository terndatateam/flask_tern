import os
from unittest import mock

import pytest


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        pytest.param(None, False),
        pytest.param(object(), False),
        pytest.param("Whatever", False),
        pytest.param("true", True),
    ],
)
def test_as_bool(value, expected):
    from flask_tern.utils.config import as_bool

    assert as_bool(value) is expected


@pytest.mark.parametrize(
    ("envvar", "value", "var", "expected"),
    [
        pytest.param("TEST_APP_SESSION_FILE_MODE", "0o755", "SESSION_FILE_MODE", 0o755),
        pytest.param("TEST_APP_SESSION_FILE_THRESHOLD", "500", "SESSION_FILE_THRESHOLD", 500),
        pytest.param("TEST_APP_DICT__KEY", "1", "DICT", {"KEY": 1}),
        pytest.param("TEST_APP_DICT", '{"KEY": 1}', "DICT", {"KEY": 1}),
    ],
)
def test_load_settings_parser(app, envvar, value, var, expected):
    from flask_tern.utils.config import load_settings

    with mock.patch.dict(os.environ, {envvar: value}):
        load_settings(app, "TEST_APP_")
        assert app.config[var] == expected
