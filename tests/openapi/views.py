import asyncio

from flask import abort, jsonify, request

from flask_tern import openapi

bp = openapi.OpenApiBlueprint("api_v1", __name__)


@bp.route("/test")
@openapi.validate()
def test():
    """This is some doc string."""
    return jsonify("ok")


@bp.route("/test/error")
@openapi.validate()
def test_error():
    """Test view which always return error."""
    code = request.openapi.parameters.query.get("code")
    if code:
        abort(code)
    raise Exception("This is an error")


@bp.route("/test/invalid_response")
@openapi.validate()
def test_invalid_response():
    """Test view that returns an invalid response according to openapi spec.

    Response validation can be configured with OPENAPI_RESPONSE_VALIDATION.
    """
    return jsonify(["a list of strings rather than an object"])


@bp.route("/test/invalid_response_on")
@openapi.validate(validate_response=True)
def test_invalid_response_on():
    """Test view that returns an invalid response according to openapi spec.

    Response validation always on and can't be changed.
    """
    return jsonify(["a list of strings rather than an object"])


@bp.route("/test/invalid_response_off")
@openapi.validate(validate_response=False)
def test_invalid_response_off():
    """Test view that returns an invalid response according to openapi spec.

    Response validation always off and can't be changed.
    """
    return jsonify(["a list of strings rather than an object"])


@bp.route("/test/async")
@openapi.validate()
async def test_async():
    """This is an async test view."""
    await asyncio.sleep(0)  # Simulating an async call.
    return jsonify("ok from async")
