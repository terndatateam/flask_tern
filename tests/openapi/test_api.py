import pytest


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test(client):
    response = client.get("/api/v1.0/test")
    assert response.status_code == 200
    assert response.json == "ok"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_error(client):
    response = client.get("/api/v1.0/test/error")
    assert response.status_code == 500
    assert response.json["message"] == "This is an error"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_http_error(client):
    response = client.get("/api/v1.0/test/error?code=404")
    assert response.status_code == 404
    assert response.json["reason"] == "Not Found"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_openapi_request_error(client):
    response = client.get("/api/v1.0/test/error?code=invalid")
    assert response.status_code == 400
    assert response.json["errors"]


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_openapi_response_error(client):
    # response not defined in openapi yaml
    response = client.get("/api/v1.0/test/error?code=401")
    assert response.status_code == 500
    assert response.json["errors"]


@pytest.mark.parametrize(
    ("config", "expected_status"),
    [
        ({"TESTING": True, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 500),
        ({"TESTING": False, "DEBUG": True, "OPENAPI_RESPONSE_VALIDATION": False}, 500),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": True}, 500),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 200),
    ],
)
@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_response_validation(config, expected_status, client):
    client.application.config.update(config)
    response = client.get("/api/v1.0/test/invalid_response")
    assert response.status_code == expected_status


@pytest.mark.parametrize(
    ("config", "expected_status"),
    [
        ({"TESTING": True, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 500),
        ({"TESTING": False, "DEBUG": True, "OPENAPI_RESPONSE_VALIDATION": False}, 500),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": True}, 500),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 500),
    ],
)
@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_response_validation_on(config, expected_status, client):
    client.application.config.update(config)
    response = client.get("/api/v1.0/test/invalid_response_on")
    assert response.status_code == expected_status


@pytest.mark.parametrize(
    ("config", "expected_status"),
    [
        ({"TESTING": True, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 200),
        ({"TESTING": False, "DEBUG": True, "OPENAPI_RESPONSE_VALIDATION": False}, 200),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": True}, 200),
        ({"TESTING": False, "DEBUG": False, "OPENAPI_RESPONSE_VALIDATION": False}, 200),
    ],
)
@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_response_validation_off(config, expected_status, client):
    client.application.config.update(config)
    response = client.get("/api/v1.0/test/invalid_response_off")
    assert response.status_code == expected_status


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_async(client):
    response = client.get("/api/v1.0/test/async")
    assert response.status_code == 200
    assert response.json == "ok from async"


# TODO: should add some parameter / response validation tests as well
#       esp. with custom error handling
