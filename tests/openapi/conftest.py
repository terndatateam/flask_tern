import pytest


@pytest.fixture
def _openapi_setup(app):
    # we get a new app each time, but it's always the same blueprint !!!!!
    from .views import bp

    app.register_blueprint(bp, url_prefix="/api/v1.0")
