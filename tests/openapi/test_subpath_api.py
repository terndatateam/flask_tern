import pytest


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test(client):
    response = client.get(
        "/api/v1.0/test",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 200
    assert response.json == "ok"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_error(client):
    response = client.get(
        "/api/v1.0/test/error",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 500
    assert response.json["message"] == "This is an error"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_http_error(client):
    response = client.get(
        "/api/v1.0/test/error?code=404",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 404
    assert response.json["reason"] == "Not Found"


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_openapi_request_error(client):
    response = client.get(
        "/api/v1.0/test/error?code=invalid",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 400
    assert response.json["errors"]


@pytest.mark.usefixtures("_openapi_setup")
def test_api_test_openapi_response_error(client):
    # response not defined in openapi yaml
    response = client.get(
        "/api/v1.0/test/error?code=401",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 500
    assert response.json["errors"]


# TODO: should add some parameter / response validation tests as well
#       esp. with custom error handling
