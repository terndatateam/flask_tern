import pytest
import yaml


@pytest.mark.usefixtures("_openapi_setup")
def test_api_root(client):
    response = client.get("/api/v1.0/")
    assert response.status_code == 302
    assert response.location.endswith("/api/v1.0/ui")


@pytest.mark.usefixtures("_openapi_setup")
def test_swagger_ui(client):
    response = client.get("/api/v1.0/ui")
    assert response.status_code == 200
    assert response.mimetype == "text/html"
    assert b"Swagger UI" in response.data


@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_yaml(client):
    response = client.get("/api/v1.0/openapi.yaml")
    assert response.status_code == 200
    assert response.mimetype == "text/yaml"
    spec = yaml.safe_load(response.data)
    assert spec["servers"][0]["url"] == "/api/v1.0/"


@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_json(client):
    response = client.get("/api/v1.0/openapi.json")
    assert response.status_code == 200
    assert response.mimetype == "application/json"
    spec = response.json
    assert spec["servers"][0]["url"] == "/api/v1.0/"


@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_json_subpath(client):
    response = client.get(
        "/api/v1.0/openapi.json",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 200
    assert response.mimetype == "application/json"
    spec = response.json
    assert spec["servers"][0]["url"] == "/desktop/api/v1.0/"


@pytest.mark.usefixtures("_openapi_setup")
def test_openapi_yaml_subpath(client):
    response = client.get(
        "/api/v1.0/openapi.yaml",
        headers={"X-Forwarded-Prefix": "/desktop"},
    )
    assert response.status_code == 200
    assert response.mimetype == "text/yaml"
    spec = yaml.safe_load(response.data)
    assert spec["servers"][0]["url"] == "/desktop/api/v1.0/"
