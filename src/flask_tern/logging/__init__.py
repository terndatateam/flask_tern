import logging
import logging.config
import sys
from datetime import datetime, timezone
from functools import wraps
from typing import Any

from flask import Flask, current_app, g, request
from werkzeug.exceptions import HTTPException

from flask_tern.auth import current_user

# AUDIT log level ... we always want to see it, so we set severity to max
AUDIT = sys.maxsize


def init_app(app: Flask):
    """Configure logging.

    This method configures the logging for the Flask application.
    It configures a logger named ``app.name``.

    If ``LOG_LEVEL`` is set, it will be used.
    If ``LOG_LEVEL`` is not set, and the application runs in debug mode, then set log level to
    ``DEBUG``. Otherwise fall back to ``INFO`` level.

    *LOG_LEVEL*: configure root logger level. This is either a string or one of the constants
    from  :ref:`python:levels`.

    Args:
        app: Flask app to configure

    .. rubric: Usage

    .. code:: python

        from flask_tern import logging as app_logging

        def create_app():
            app = Flask(__name__)
            app_logging.init_app(app)
            return app

    """
    # Add new AUDIT log level, which has highest priority (always log it)
    logging.addLevelName(AUDIT, "AUDIT")
    # set log level on app logger
    log_level = app.config.get("LOG_LEVEL")
    if log_level:
        # if log level is set use it
        logging.getLogger(app.name).setLevel(log_level)
    elif app.debug:
        # if log level is not set and debug mode is on set log_level to DEBUG
        logging.getLogger(app.name).setLevel("DEBUG")
    else:
        # fall back to INFO
        logging.getLogger(app.name).setLevel("INFO")
    # create app logger (this is our app root logger)
    # this will add a handler as well if needed
    app.logger.debug("LOGGING setup done")


def log_audit(*args, **kwargs):
    """Use this method to log audit messages.

    Parameters are passed on to python log function.
    Usually the result of create_audit_event should be passed in as the only parameter.

    Args:
        args: positional args passed to log function.
        kwargs: keyword args passed to log function.
    """
    logger = logging.getLogger("audit")
    logger.log(AUDIT, *args, **kwargs)


def record_audit_target(target_id, **kw):
    """Store target information in current application context.

    The :py:meth:`audit_log` decorator will pick up this information and add it to the audit
    log object.

    The information is stored in :py:data:`flask:flask.g` under key ``audit_target``.

    Args:
        target_id: identifier for target
        kw: additional properties added to target
    """
    g.audit_target = {"id": target_id, **kw}


def audit_ignore(audit_ignore=True):
    """Call to disable audit log via :py:meth:`audit_log` decorator.

    This is useful in case automatic audit logging should be turned off for this request.

    Args:
        audit_ignore: Sets the request global g.audit_ignore flag to ``True`` or ``False``
    """
    g.audit_ignore = audit_ignore


def audit_success(success=True, reason=None):
    """Call to set audit_outcome manually.

    Args:
        success: If true outcome will be "success", else outcome will be "failure".
        reason: A dictionary containing "reasonType" and "reasonCode" and/or "policyType"
                and "policyId"
    """
    if success:
        g.audit_outcome = "success"
    else:
        g.audit_outcome = "failure"
    if reason:
        g.audit_reason = reason


def create_audit_event(action: str, outcome, target: str | dict | None = None) -> dict:
    """Create an Audit Event object (dictionary).

    The resulting object can be passed to log_audit directly.

    Args:
        action: audit log action field
        outcome: audit outcome
        target: if a string it's used as target id,
                if a dict, it's the full target section in the audit log

    Returns:
        full audit message as dictionary
    """
    # build initiator (current user)
    initiator: dict[str, str | Any] = {
        "id": None,
        "credential": None,
        # some other client details
        "host": {
            "agent": str(request.user_agent),
            "ip": request.remote_addr,
        },
    }
    if current_user:
        initiator["id"] = current_user.id
        # TODO: name, email?
        initiator["credential"] = {"type": g.auth_type}

    event = {
        "typeURI": "http://schemas.dmtf.org/cloud/audit/1.0/event",
        "action": action,
        "outcome": outcome,
        "eventTime": datetime.now(timezone.utc).isoformat(),
        "observer": {
            # app details
            "id": request.url_root,  # observer same as target
            "addresses": {
                "name": current_app.name,
                "url": request.url_root,
            },
        },
        "initiator": initiator,
        "target": {
            # TODO: where do I identify the actual resource? (e.g. file download, pkg etc...)
            # "typeURI": "service/bss/ecoimages",
            "addresses": [
                {
                    "name": request.url_rule.endpoint,
                    "url": request.url,
                },
            ],
        },
        # reporterchain: []
        # tags: ... could use request id from ingress here?
        # TODO: any additional things?
    }
    # update target if it has been given as parameter
    if target:
        if isinstance(target, dict):
            event["target"].update(target)
        else:
            event["target"]["id"] = target
    # update target if it has been recorded in g
    if g.get("audit_target"):
        event["target"].update(g.audit_target)
    # update reason if it's set
    if g.get("audit_reason"):
        event["reason"] = g.audit_reason
    return event


def audit_log(action: str, target: str | dict | None = None):
    """Wrap view methods to create audit logs.

    Args:
        action: Sets the action field of the audit message
        target: Either a target id or a :py:class:`python:dict` with fields to put into
                ``target`` of audit message., optional

    This method automatically sets the ``outcome`` field to ``success`` or in case the view method
    throws an exception to ``failure``.
    Use :py:func:`record_audit_target` to add additional information about the target.

    # noqa: DAR201 # don't validate return
    """

    def decorator(view_func):
        @wraps(view_func)
        def decorated(*args, **kwargs):
            audit_success()
            try:
                return view_func(*args, **kwargs)
            except HTTPException as e:
                audit_success(
                    False, {"reasonType": "HTTP", "reasonCode": e.code, "message": e.description}
                )
                raise e
            except Exception as e:
                audit_success(False, {"reasonType": "HTTP", "reasonCode": 500, "message": str(e)})
                raise e
            finally:
                if not g.get("audit_ignore", False):
                    log_audit(create_audit_event(action, g.audit_outcome, target))

        return decorated

    return decorator
