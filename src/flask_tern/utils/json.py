from datetime import date, datetime, time

from flask.json.provider import DefaultJSONProvider


class TernJSONProvider(DefaultJSONProvider):
    """Custom json provider.

    This is a custom JSONProvider to return to encode date, time and datetime instance as ISO8601 strings.

    .. rubric:: Usage

    .. code::

        app = Flask(__name__)
        app.json = TernJSONProvider(app)
    """

    @staticmethod
    def default(o):
        """Format given values to json.

        If given value is a ``date``, ``datetime`` or ``time`` instance, format it is ISO 8601 string.
        Otherwise fall back to super class encoding.

        Args:
            o: Value to serialise

        Returns:
            JSON formatted value
        """
        if isinstance(o, (date, datetime, time)):
            return o.isoformat()
        return super(TernJSONProvider, TernJSONProvider).default(o)
