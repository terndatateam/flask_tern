from collections.abc import Callable
from typing import Any

import flask

# TODO: optional import?
import redis


def load_settings(
    app: flask.Flask,
    env_prefix: str = "FLASK_TERN_",
    defaults: dict | None = None,
    config: dict | None = None,
):
    """Function intended to be used to configure a Flask app instance.

    Args:
        app: The flask app to configure.
        env_prefix: Only env var prefixed with this are considered as configuration.
        defaults: Only env var prefixed with this are considered as configuration.
        config: object with highest priority config values.

    Options are applied in the following order to the app instance. Later steps override values
    form previous steps.
    This method loads settings via :py:meth:`flask.Config.from_object`.

        1. apply defaults from bundled extensions
        2. apply defaults in :py:mod:`flask_tern.settings`
        3. passed in defaults
        4. call :py:meth:`flask:flask.Config.from_prefixed_env`
        5. call :py:func:`convert_settings`
        6. apply passed in config

    .. rubric:: Usage

    .. code:: python

      app = Flask(__name__)
      load_settings(app, "MY_APP_", {}, {})

    """
    # 1. load module defaults
    from flask_tern import settings as default_settings
    from flask_tern.auth import settings as auth_settings
    from flask_tern.elasticsearch import settings as es_settings
    from flask_tern.openapi import settings as openapi_settings
    from flask_tern.proxyfix import settings as proxyfix_settings

    app.config.from_object(auth_settings)
    app.config.from_object(es_settings)
    app.config.from_object(openapi_settings)
    app.config.from_object(default_settings)
    app.config.from_object(proxyfix_settings)
    # 2. apply defaults
    if defaults:
        app.config.from_object(defaults)
    # 3. apply config file
    app.config.from_envvar("{}SETTINGS".format(env_prefix), silent=True)
    # 4. load env vars
    app.config.from_prefixed_env(env_prefix.rstrip("_"))
    # 5. apply passed in config
    convert_settings(app)
    # 6. apply passed in config
    if config:
        app.config.update(config)


def as_bool(value: str | bool) -> bool:
    """Convert value to bool.

    Accepts ``True``, ``1`` and ``on`` as True and everything else as false.
    It uses case insensitive comparison.

    Args:
        value: string interpret

    Returns:
        Whether string is to be interpreted as ``True`` or ``False``
    """
    if not value:
        # empty or None is False
        return False

    if isinstance(value, bool):
        # already a bool, return as is
        return value

    if not isinstance(value, str):
        # no a string
        return False

    return value.lower() in ("true", "1", "on")


##########################################################
# Parse environment
##########################################################

# TODO: maybe this should be in settings.py?
#       or add API to add custom parsers?
# This map configures how some well known configuration options need to be parsed
# from string (environment)
_settings_type_map = {
    # session settings
    "SESSION_COOKIE_HTTPONLY": as_bool,
    "SESSION_COOKIE_SECURE": as_bool,
    "SESSION_PERMANENT": as_bool,
    "SESSION_REDIS": redis.from_url,
    "SESSION_FILE_THRESHOLD": int,
    # convert string as octal number ...
    "SESSION_FILE_MODE": lambda x: x if isinstance(x, int) else int(x, base=8),
    # SQLAlchemy
    "SQLALCHEMY_TRACK_MODIFICATIONS": as_bool,
    # Elasticsearch
    "ELASTICSEARCH_VERIFY_CERTS": as_bool,
    "ELASTICSEARCH_SSL_SHOW_WARN": as_bool,
    "ELASTICSEARCH_REQUEST_TIMEOUT": int,
    # OIDC
    "OIDC_USE_REFRESH_TOKEN": as_bool,
    # OpenApi
    "OPENAPI_RESPONSE_VALIDATION": as_bool,
}


def add_setting_parser(name: str, parser: Callable[[str], Any]):
    """Adds a parser from environment variable to settings data type.

    Args:
        name: setting name
        parser: function to convert string from environment to required data type

    This allows applications to easily add custom parsers for extra options.
    The provided parser function could do anything. E.g. parse the value as json,
    interpret it as filename, etc...

    The parser method will be called with the value of the environment variable (may be empty,
    but should never be ``None``). The return value can be anything which will eventually be stored
    in Flask application configuration (:doc:`flask:config`) under setting name.

    .. rubric:: Usage

    .. code:: python

        # my_app.settings.py
        from flask_tern.utils.config import add_setting_parser

        # set default for MAX_COUNT
        MAX_COUNT = 2
        # add parser for this which converts ``str`` to ``int``
        add_setting_parser("MAX_COUNT", int)

    """


def convert_settings(app: flask.Flask, type_map: dict = _settings_type_map):
    """Goes through registered value converters and tries to parse into python type.

    Args:
        app: The flask app to configure.
        type_map: a mapping from option name to some method used to parse value,
                  and convert to a python type.
    """
    for key, convert in type_map.items():
        # TODO: would be nice if we could do keys like 'var.*.item' to match all items or so
        #       maybe jsonpath would be good here ?
        if app.config.get(key):
            # value is in config and it is not None
            app.config[key] = convert(app.config[key])
