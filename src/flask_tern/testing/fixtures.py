"""This module defines some re-usable test fixtures.

These test fixtures are useful to make testing apps built with this extension a little bit easier.
"""

import base64

import pytest
from flask import jsonify


# TODO: this fixture should be part of pytest 5.3.6
@pytest.fixture(scope="session")
def monkeypatch_session(request):
    """Experimental (https://github.com/pytest-dev/pytest/issues/363)."""
    from _pytest.monkeypatch import MonkeyPatch

    mpatch = MonkeyPatch()
    try:
        yield mpatch
    finally:
        mpatch.undo()


# a fixture to cache openapi spec parsing and validation
# this is a session based fixture with autouse enabled.
@pytest.fixture(scope="session", autouse=True)
def _cache_spec(monkeypatch_session):
    """Cache openapi spec for the whole test session.

    Initial parsing of openapi spec is really slow.
    """
    from flask_tern.openapi import blueprint

    orig = blueprint.load_openapi_spec
    cached_spec = None

    def my_load_spec(state):
        nonlocal cached_spec
        if cached_spec is None:
            orig(state)
            cached_spec = state.blueprint.spec
        else:
            state.blueprint.spec = cached_spec

    monkeypatch_session.setattr(blueprint, "load_openapi_spec", my_load_spec)


# TODO: add method to generate invalid user credentials (easier in testing)
@pytest.fixture
def basic_auth(app) -> dict:
    """A fixture to set up some well defined users which can be used with basic authentication during testing.

    Returns:
        Dictionary with users and their attributes available for testing.

    .. rubric:: Usage

    .. code:: python

        def my_test_func(client, basic_auth):
            r = client.post(
                "/api/url",
                headers={"Authorization"}: basic_auth["user"]["auth"]},
            )
    """
    # A fixture to configure basic authentication and some users for testing
    from flask_tern.auth import User

    class TestUser(User):
        auth: str | None

    user_db = {
        "admin": TestUser(
            id="admin",
            name="admin",
            email="admin@example.com",
            roles=["admin"],
        ),
        "user": TestUser(
            id="user",
            name="user",
            email="user@example.com",
            roles=["user"],
        ),
        "service": TestUser(
            id="service",
            name="service",
            roles=["service"],
        ),
    }

    for key, value in user_db.items():
        user_db[key].auth = "Basic {}".format(
            base64.b64encode("{}:{}".format(value.name, value.name).encode("ascii")).decode("ascii")
        )

    def dev_userinfo_from_basic(auth_info):
        if auth_info["username"] in user_db:
            return user_db[auth_info["username"]]
        return None

    app.config["USERINFO_MAP"]["basic"] = dev_userinfo_from_basic
    return user_db


@pytest.fixture
def db(app):
    """Configure and return Flask-SQLAlchemy extensions.

    If ``SQLALCHEMY_URL`` is configured globally, then this fixture is not needed.
    It is useful to enable SQLAlchemy extension conditionally for specific tests.

    .. todo::
        Check if this fixture is really useful in apps. (Seems like it's only useful for this library itself.)

    Returns:
        Instance of Flask-SQLAlchemy extension configured for testing.
    """
    # setup SQLAlchemy extension
    from flask_tern import db

    db.init_app(app)
    # setup db
    with app.app_context():
        db.db.drop_all()
        db.db.create_all()
        # here we would set up initial data for all tests if needed
    return db


@pytest.fixture
def _oidc(app):
    # setup oidc extension
    # TODO: Check if this fixture is really useful in apps. (Seems like it's only useful for this
    #       library itself.)
    from flask_tern import auth
    from flask_tern.auth.login import oidc_login

    # init oauth
    auth.init_app(app)
    # setup oidc_login views

    app.register_blueprint(oidc_login, url_prefix="/api/oidc")


@pytest.fixture
def cache(app):
    """Configure and return Flask-Caching extensions.

    Configures ``CACHE_TYPE = "flask_caching.backends.SimpleCache"`` and returns the Flask-Caching
    instance to use in tests.

    .. todo::
        Check if this fixture is really useful in apps. (Seems like it's only useful for this library itself.)

    Returns:
        Instance of Flask-Caching extension configured for testing.
    """
    # setup cache extension
    # TODO: may be required for oidc ?
    from flask_tern import cache

    app.config["CACHE_TYPE"] = "flask_caching.backends.SimpleCache"

    cache.init_app(app)
    return cache.cache


@pytest.fixture
def views(app) -> dict:
    """Add some simple views for testing to Flask app.

    * `/public`: returns `ok`
    * `/`: endpoint named `root` and returns `ok`
    * `/private`: requires valid user, and returns user dict

    .. todo::
        Check if this fixture is really useful in apps. (Seems like it's only useful for this library itself.)

    .. rubric:: Usage

    .. code:: python

        from flask_tern.testing.fixtures import views

        def test_view(client, views):
            response = client.get(views["public"]) # root, private
            assert response.status_code = 200
            assert response.text = "ok"

    Returns:
        Mapping for view name to url to use in tests.
    """
    # define some simple views for testing
    from flask_tern.auth import current_user
    from flask_tern.auth.decorators import require_user

    def public_view():
        return jsonify("ok")

    @require_user
    def private_view():
        if not current_user:
            return None

        return jsonify(current_user._get_current_object())

    app.add_url_rule("/public", view_func=public_view)
    app.add_url_rule("/", endpoint="root", view_func=public_view)
    app.add_url_rule("/private", view_func=private_view)
    return {"public": "/public", "root": "/", "private": "/private"}
