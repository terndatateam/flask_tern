import os.path

import flask

import flask_tern.logging


def init_app(app: flask.Flask):
    """A helper method that initalises all included extensions based on config options.

    Args:
        app: The Flask app to configure

    - Logging: always enabled
    - SQLAlchemy: enabled if ``SQLALCHEMY_DATABASE_URI`` is set
    - Elasticsearch: enabled if ``ELASTICSEARCH_URL`` is set
    - Session: enabled if ``SESSION_TYPE`` is set
    - Cache: always enabled
    - CORS: always enabled
    - ProxyFix: always enabled
    - HealthCheck: adds builtin check methods if the respective configuration is set

        - ``SQLALCHEMY_DATABASE_URI``: add :py:func:`.healthcheck.check.check_db`
        - ``ELASTICSEARCH_URL``: add :py:func:`healthcheck.checks.check_es`
        - ``APIKEY_SERVICE``: add :py:func:`healtcheck.checks.check_apikey`
        - ``OIDC_DISCOVERY_URL``: add :py:func:`healthcheck.checks.check_keycloak``
        - ``DOI_URL``: add :py:func:`healthcheck.checks.check_doi``
    - OIDC Auth: enabled if ``ODIC_CLIENT_ID`` is set.
    """
    ################################################################
    # Configure logging
    ################################################################
    flask_tern.logging.init_app(app)

    #################################################################
    # Configure various Flask extensions used by this app
    #################################################################
    from flask_tern import cache

    cache.init_app(app)

    #################################################################
    # Configure sqlalchemy ad alembic
    #################################################################
    # Register extensions
    if app.config.get("SQLALCHEMY_DATABASE_URI"):
        from flask_migrate import Migrate

        from flask_tern import db

        # api.init_app(app)
        db.init_app(app)
        Migrate(app, db.db, directory=os.path.join(app.config.root_path, "migrations"))

    ###############################################
    # Session setup
    ###############################################
    if app.config.get("SESSION_TYPE"):
        # only configure Flask-Session if requested, fall back to falsk Secure Cookie Session.
        from flask_session import Session
        from flask_session.defaults import Defaults as SessionDefaults

        if app.config.get("SESSION_TYPE") == "filesystem":
            # session type filesystem has ben requested, so we setup a cachelib backed session
            # with file system backend.
            app.config["SESSION_TYPE"] = "cachelib"
            from cachelib import FileSystemCache

            app.config["SESSION_CACHELIB"] = FileSystemCache(
                cache_dir=app.config.get("SESSION_FILE_DIR", SessionDefaults.SESSION_FILE_DIR),
                threshold=app.config.get(
                    "SESSION_FILE_THRESHOLD", SessionDefaults.SESSION_FILE_THRESHOLD
                ),
                mode=app.config.get("SESSION_FILE_MODE", SessionDefaults.SESSION_FILE_MODE),
            )

        Session(app)

    #################################################################
    # Configure elasticsearch
    #################################################################
    if app.config.get("ELASTICSEARCH_URL"):
        from flask_tern import elasticsearch

        elasticsearch.init_app(app)

    ###############################################
    # CORS
    ###############################################
    from flask_cors import CORS

    CORS(app)

    ###############################################
    # ProxyFix
    ###############################################
    from flask_tern import proxyfix

    proxyfix.init_app(app)

    ###############################################
    # Healthcheck
    ###############################################
    from flask_tern import healthcheck
    from flask_tern.healthcheck import checks

    healthcheck.init_app(app)

    #############################################
    # Setup OIDC
    #############################################
    if app.config.get("OIDC_CLIENT_ID"):
        from flask_tern import auth

        auth.init_app(app)
