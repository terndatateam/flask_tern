"""Setup ProxyFix middleware.

For details please see :doc:`flask:deploying/proxy_fix`.

ProxyFix middleware is wrapped here to make it configurable via environment variables.

See py:mod:`flask_tern.proxyfix.settings`.

The following headers are considered by ProxyFix middleware:

    - `X-Forwarded-For` sets `REMOTE_ADDR`.
    - `X-Forwarded-Proto` sets `wsgi.url_scheme`.
    - `X-Forwarded-Host` sets `HTTP_HOST`, `SERVER_NAME`, and `SERVER_PORT`.
    - `X-Forwarded-Port` sets `HTTP_HOST` and `SERVER_PORT`.
    - `X-Forwarded-Prefix` sets `SCRIPT_NAME`.

"""

import flask
from werkzeug.middleware.proxy_fix import ProxyFix


def init_app(app: flask.Flask):
    """This method is used to configure :doc:`flask:deploying/proxy_fix` middleware.

    It sets the number of values for various `X-Forward-XXX` header values considered by the middleware.

    .. rubric:: Usage

    .. code:: python

        from flask_tern import proxyfix

        def create_app():
            app = Flask(__name__)
            proxyfix.init_app(app)
            return app

    Args:
        app: The flask app to configure.

    """
    x_for = app.config.get("PROXYFIX_X_FOR", 1)
    x_proto = app.config.get("PROXYFIX_X_PROTO", 1)
    x_host = app.config.get("PROXYFIX_X_HOST", 0)
    x_prefix = app.config.get("PROXYFIX_X_PREFIX", 0)
    x_port = app.config.get("PROXYFIX_X_PORT", 0)

    app.wsgi_app = ProxyFix(
        app.wsgi_app, x_for=x_for, x_proto=x_proto, x_host=x_host, x_port=x_port, x_prefix=x_prefix
    )
