##########################################################
# ProxyFix settings
##########################################################

#: Number of values to trust for X-Forwarded-For.
PROXYFIX_X_FOR = 1
#: Number of values to trust for X-Forwarded-Proto.
PROXYFIX_X_PROTO = 1
#: Number of values to trust for X-Forwarded-Host.
PROXYFIX_X_HOST = 0
#: Number of values to trust for X-Forwarded-Port.
PROXYFIX_X_PORT = 0
#: Number of values to trust for X-Forwarded-Prefix.
PROXYFIX_X_PREFIX = 0
