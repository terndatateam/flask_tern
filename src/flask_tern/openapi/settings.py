##########################################################
# OpenAPI settings
##########################################################

#: The Swagger UI version used to show the openapi documentation.
SWAGGER_UI_VERSION = "5.0.0-alpha.14"
#: If True enforce OPEN API response validation. Response validation is always active during
#: test runs and when flask is run with `--debug`.
OPENAPI_RESPONSE_VALIDATION = False
