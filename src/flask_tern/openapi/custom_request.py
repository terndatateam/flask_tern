from openapi_core.contrib.flask.requests import FlaskOpenAPIRequest


class TernFlaskOpenAPIRequest(FlaskOpenAPIRequest):
    """Custom FlaskOpenAPIRequest that overrides the path_pattern property.

    This subclass uses the Flask route (from the URL rule) and converts Flask-style
    parameters (e.g. <id>) into OpenAPI-style parameters (e.g. {id}). It also removes
    any extra subpath from the request's root_path so that the remaining route is
    relative to the blueprint.
    """

    def get_path(self, path: str) -> str:
        """Return request path to use to match against openapi route.

        This overrides the original 'get_path' method which injects the `request.root_path`
        into the api route path.

        Our openapi spec api routes are relative to the blueprint.
        Se the path to use to find the proper api path in the openapi spec needs to be without
        any `request.root_path` prepended.

        The value of `request.roo_path` and `request.path` is set by respecting headers like
        `X-Forwared-Prefix` (handled by proxyfix extension).

        Args:
            path: the current `request.path`

        Returns:
            path used to find openapi route for blueprint.
        """
        # Leave unchanged ... the path is correct as it is.
        return path
