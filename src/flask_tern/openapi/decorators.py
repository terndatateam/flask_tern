# flake8: noqa: D202
import inspect
import logging
from functools import wraps

from flask import abort, after_this_request, current_app, request
from openapi_core import V3RequestUnmarshaller, V3ResponseUnmarshaller
from openapi_core.contrib.flask import FlaskOpenAPIResponse

# TODO: probably want my own error response builder here
from openapi_core.contrib.flask.handlers import FlaskOpenAPIErrorsHandler

from flask_tern.openapi.custom_request import TernFlaskOpenAPIRequest

logger = logging.getLogger(__name__)


def _validate_request():
    spec = current_app.blueprints[request.blueprint].spec
    # # TODO: How doas FlaskOpenApiRequest handle uploads? e.g. it just puts request.data in
    oai_request = TernFlaskOpenAPIRequest(request)
    # validate request
    validator = V3RequestUnmarshaller(spec=spec)
    validation_result = validator.unmarshal(oai_request)

    # put validation result onto request
    request.openapi = validation_result

    # return error response in case validation fails
    if validation_result and validation_result.errors:
        error_handler = FlaskOpenAPIErrorsHandler()
        response = error_handler(validation_result.errors)
        # Logging the error
        logger.error("OpenAPI validation errors: %s", validation_result.errors)
        abort(response.status_code, response=response)


def _validate_response(response):
    # TODO: check this ... this turns off validation of 500 responses
    # if response is already 500 then just return it ....
    if response.status_code == 500:
        return response
    # otherwise validate it
    spec = current_app.blueprints[request.blueprint].spec

    # # TODO: should we store oai_request somewhere instead of recreating it here?
    oai_request = TernFlaskOpenAPIRequest(request)

    oai_response = FlaskOpenAPIResponse(response)

    # need to set base_url, otherwise openapi validation complains about `Server not found`
    # it's needed to geth the relative paths in openapi.yaml right
    # (relative to actual mount point in flask app)
    # TODO: add test for base_url
    validator = V3ResponseUnmarshaller(spec=spec, base_url=request.host_url)
    validation_result = validator.unmarshal(oai_request, oai_response)
    if validation_result and validation_result.errors:
        flask_openapi_errors_handler = FlaskOpenAPIErrorsHandler()
        new_response = flask_openapi_errors_handler(validation_result.errors)
        # change response in case we get validation error
        # -> always turn into 500 error ... server erred
        # TODO: we don't really know which format response has, all we know is that
        #       this response did not validate against openapi spec
        # new_response ... oai validation error
        # response ... invalid response according to oai spec
        new_response.status_code = 500
        # don't raise it, this runs in after_this_request handler
        return new_response
    return response


def validate(validate_request=True, validate_response=None):
    """Decorate view methods to enable openapi request / response validation.

    Using this decorator enforces request and/or response validation against the openapi spec.

    Args:
        validate_request: enable / disable request validation
        validate_response: enable / disable response validation

    Modifications:
        - v0.14.0: Added support for asynchronous view functions by checking if the view function is a coroutine.

    # noqa: DAR201 ignore missing returns
    """

    # TODO: check if in prod mode then force validate_response to False?
    #       maybe via an app.config entry?
    def decorator(view_func):
        if inspect.iscoroutinefunction(view_func):

            @wraps(view_func)
            async def async_decorated(*args, **kwargs):
                if validate_request:
                    _validate_request()
                # if validate_response is set to True then validate
                # if validate_response is set to False then don't validate
                # if validate_response is unset (None) check if we run in debug or test mode
                #    otherwise use OPENAPI_RESPONSE_VALIDATION flag as default.
                if (
                    validate_response
                    or validate_response is None
                    and (
                        current_app.debug
                        or current_app.testing
                        or current_app.config.get("OPENAPI_RESPONSE_VALIDATION", False)
                    )
                ):
                    after_this_request(_validate_response)
                # Await the asynchronous view function.
                return await view_func(*args, **kwargs)

            return async_decorated
        else:

            @wraps(view_func)
            def sync_decorated(*args, **kwargs):
                if validate_request:
                    _validate_request()
                # if validate_response is set to True then validate
                # if validate_response is set to False then don't validate
                # if validate_response is unset (None) check if we run in debug or test mode
                #    otherwise use OPENAPI_RESPONSE_VALIDATION flag as default.
                if (
                    validate_response
                    or validate_response is None
                    and (
                        current_app.debug
                        or current_app.testing
                        or current_app.config.get("OPENAPI_RESPONSE_VALIDATION", False)
                    )
                ):
                    after_this_request(_validate_response)
                return view_func(*args, **kwargs)

            return sync_decorated

    return decorator
