import os

import yaml
from flask import (
    Blueprint,
    current_app,
    jsonify,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
)
from flask.helpers import get_root_path
from jinja2 import FileSystemLoader
from jsonschema_path import SchemaPath
from werkzeug.exceptions import HTTPException
from werkzeug.utils import cached_property


def OpenApiBlueprint(name: str, import_name: str) -> Blueprint:
    """A Blueprint factory to serve APIs described by an OpenAPI spec document.

    It provides a few routes per default:

    - **/**: A route named ``api_root`` which redirects to the Swagger UI endpoint.
    - **/ui**: a route to render a Swagger UI page
    - **/openapi.yaml**: serves the openapi document as yaml
    - **/openapi.json**: serves the openapi document as yaml

    The openapi.yaml file is loaded via py:func:`flask:flask.Blueprint.open_resource`

    Args:
        name: prefix for route names registered at blueprint
        import_name: module name used to locate resources

    Returns:
        Blueprint instance
    """
    bp = _OpenApiBlueprint(name, import_name)

    bp.record(load_openapi_spec)

    # add default routes:
    bp.add_url_rule("/", "api_root", api_root)
    bp.add_url_rule("/ui", "swagger_ui", swagger_ui)
    bp.add_url_rule("/openapi.yaml", "openapi_yaml", openapi_yaml)
    bp.add_url_rule("/openapi.json", "openapi_json", openapi_json)

    # register default error handler
    bp.register_error_handler(Exception, _openapi_error_handler)
    return bp


class _OpenApiBlueprint(Blueprint):
    """A custom blueprint, to properly load jinja template assets.

    Make sure templates are loaded from where Blueprint is instantiated with a fallback,
    to this package.
    """

    # TODO: deprecated ??
    @cached_property
    def jinja_loader(self):
        # get configured loader
        search_path = []
        if self.template_folder is not None:
            search_path.append(os.path.join(self.root_path, self.template_folder))
        # add this template folder as well
        search_path.append(
            os.path.join(
                get_root_path(__name__),
                "templates",
            )
        )
        return FileSystemLoader(search_path)


def _openapi_error_handler(e):
    # inspired by https://cloud.google.com/storage/docs/json_api/v1/status-codes
    # TODO: should we take custom headers into account somehow?
    # TODO: would be nice if we could set multiple errors somehow
    #       e.g. very useful for from submissions (custom exceptions (attributes)?, global variables?)
    #            special handling of some exceptions? (e.g. 400 bad request)
    error = None
    if isinstance(e, HTTPException):
        # take code and messag from http exception
        error = {
            "code": e.code,  # HTTP code
            "reason": e.name,  # HTTP reason
            "message": e.description,  # error message
            # TODO: errors would be nice to have
            #       with even more detail
            # "errors": {...}
        }
        # in case of openapi validation errors we should have a response
        if e.response and e.response.is_json and e.response.json.get("errors"):
            error["errors"] = e.response.json["errors"]
    else:
        # e is some other generic exception
        error = {
            "code": 500,
            "reason": "Internal Server Error",
            "message": str(e),
        }
    return (
        jsonify(error),
        error["code"],
    )


def load_openapi_spec(state):
    """Load `openapi.yaml` from current blueprint resources folder.

    Args:
        state: Blueprint initialisation state.
    """
    # load openapi yaml from current blueprint resources folder
    with state.blueprint.open_resource("openapi.yaml") as oai:
        spec = yaml.safe_load(oai)
    # inject the blueprint mount path into the openapi spec.
    spec["servers"] = [{"url": state.url_prefix or "/"}]
    # cache loaded openapi spec as is
    state.blueprint.spec_dict = spec
    # cache parsed openapi spec (parsing cane be expensive)
    state.blueprint.spec = SchemaPath.from_dict(spec)


def api_root():
    """Serves as anchor to use url_for when generating openapi.yaml otherwise redirects to swagger ui.

    Returns:
        Redirect to Swagger UI url
    """
    return redirect(url_for(".swagger_ui"))


def swagger_ui():
    """View function to render swagger UI."""
    variables = {
        "spec_url": "openapi.yaml",
        "ui_version": current_app.config["SWAGGER_UI_VERSION"],
    }
    return render_template("swagger_ui.html", **variables)


def openapi_yaml():
    """View function to render current OpenAPI spec as yaml."""
    bp = current_app.blueprints[request.blueprint]
    # make a shallow copy and inject external api path in case we are hosted under a subpath
    spec_dict = {
        **bp.spec_dict.copy(),
        # Swagger UI requires correct servers base url
        "servers": [{"url": url_for(".api_root")}],
    }
    return make_response(yaml.safe_dump(spec_dict), {"Content-Type": "text/yaml; charset=utf-8"})


def openapi_json():
    """View function to render current OpenAPI spec as json."""
    bp = current_app.blueprints[request.blueprint]
    # make a shallow copy and inject external api path in case we are hosted under a subpath
    spec_dict = {
        **bp.spec_dict.copy(),
        # Swagger UI requires correct servers base url
        "servers": [{"url": url_for(".api_root")}],
    }
    return jsonify(spec_dict)
