from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import declarative_base

#: We define our own declarative base class. Using this base class makes it possible to re-use
#: db models outside of flask context
DeclarativeBase = declarative_base()  # cls=ModelBase)


#: global :py:class:`flasksa:flask_sqlalchemy.SQLAlchemy` object.
db = SQLAlchemy(model_class=DeclarativeBase)


def init_app(app):
    """Initialise :doc:`flasksa:index` extension."""
    # little hack to avoid issues with our defaults an sqlite
    if app.config["SQLALCHEMY_DATABASE_URI"].startswith("sqlite:"):
        # remove pool_size option in case of sqlite
        app.config["SQLALCHEMY_ENGINE_OPTIONS"].pop("pool_size", None)
    db.init_app(app)
