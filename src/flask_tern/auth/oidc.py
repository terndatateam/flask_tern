import logging

from authlib.integrations.flask_client import OAuth
from authlib.jose import jwt
from flask import session

from flask_tern.auth.user import User

# We need a global instance which we can use anywhere
oauth = OAuth()


# TODO: what about bearer auth? ... we need to get the access_token in here somehow from auth header
def fetch_token(name):
    """Used by OAuth registry to fetch tokens from session."""
    # name: name of registered oidc client
    # load current tokens from session
    return session.get("oidc.tokens")
    # NOTE: this stores the tokens on app context as well (on first access)


def update_token(name, token, refresh_token=None, access_token=None):
    """Used by OAuth registry to update tokens in session.

    Args:
        name: name of registered oidc client
        token: updated tokens
        refresh_token: old refresh token if supplied?
        access_token: old access token if supplied?
    """
    # get current tokens
    tokens = session["oidc.tokens"]
    # TODO: refresh_token and access_token can be used to fetch existing stored tokens (for update)
    #       should we at least validate these here?
    # if refresh_token and tokens["refresh_token"] != refresh_token:
    #     print("New refresh token different to old token")
    # elif access_token and tokens["access_token"] != access_token:
    #     print("New access token different to old token")
    # else:
    #     print("NO TOKEN IN UPDATE")

    # update session (need a new dict so that session knows we have new data)
    logging.getLogger(__name__).debug("TOKEN UPDATE: %s", token)
    session["oidc.tokens"] = {**tokens, **token}


def get_oauth_userinfo():
    """Extract UserInfo from current tokens."""
    log = logging.getLogger(__name__)
    try:
        # decode and validate id token
        md = oauth.oidc.load_server_metadata()
        claims_options = {
            "iss": {"essential": True, "values": [md["issuer"]]},
            "aud": {"essential": True, "values": [oauth.oidc.client_id]},
        }
        # 1. get parsed id_token
        # authlib 1.0.0 parses id_token into user_info
        id_token = oauth.oidc.token["userinfo"]
        # 2. parse and validate access token
        jwks = oauth.oidc.fetch_jwk_set()
        claims = jwt.decode(
            oauth.oidc.token["access_token"],
            key=jwks,
            claims_options=claims_options,
            claims_params={},
        )
        claims.validate()
        # 3. combine infos
        user_info = dict(claims)
        user_info.update(id_token)
        return User(
            # TODO: ideally the user id attribute would be configurable and if it is missing
            #       then we can't create a user.
            id=user_info["sub"],
            # TODO: name has to be configurable as well (e.g. preferred_username?)
            #       for now fall back to id
            name=user_info.get("name", user_info["sub"]),
            email=user_info.get("email"),
            email_verified=user_info.get("email_verified", False),
            roles=(
                user_info.get("resource_access", {}).get(oauth.oidc.client_id, {}).get("roles", [])
            ),
            # TODO: scope is a mandatory attribute in OIDC
            #       here it is treated as optional.
            scopes=[x.strip() for x in user_info.get("scope", "").split(" ") if x.strip()],
            claims=user_info,
        )
    except Exception:
        log.exception("Error decoding access token")
    return None


# TODO: overlap with above method?
def get_userinfo_from_accesstoken(auth_info) -> User | None:
    """Extract user information and roles from access token.

    Args:
        auth_info: dict, A dictionary as returned by
                   :py:func:`flask_tern.auth.utils.get_authorization`. With keys
                   `type`: `bearer` and `token`: the encoded access token.

    Returns:
        'User' object with attributes set from auth_info
    """
    log = logging.getLogger(__name__)
    # We assume that the bearer token is a JWT.
    # TODO: if not we would need to invoke OIDC servers token introspection endpoint
    #       or get info about token by some other means.
    try:
        # These calls are cached forever on the oauth.oidc instance
        md = oauth.oidc.load_server_metadata()
        jwks = oauth.oidc.fetch_jwk_set()
        # decode and validate JWT
        # minimum set to validate is issuer and claim, to make sure
        # token came from trusted source (iss), and is meant for us (aud)
        claims_options = {
            "iss": {"essential": True, "values": [md["issuer"]]},
            "aud": {"essential": True, "values": [oauth.oidc.client_id]},
        }
        claims = jwt.decode(
            auth_info["token"],
            key=jwks,
            claims_options=claims_options,
            claims_params={},
        )
        claims.validate()
        return User(
            id=claims.get("sub"),
            name=claims.get("name"),  # optional .. e.g. service accounts don't have it
            email=claims.get("email"),  # optional .. e.g. service accounts don't have it
            email_verified=claims.get(
                "email_verified"
            ),  # optional .. e.g. service accounts don't have it
            roles=(
                claims.get("resource_access", {}).get(oauth.oidc.client_id, {}).get("roles", [])
            ),
            scopes=[x.strip() for x in claims.get("scope").split(" ") if x.strip()],
            claims=dict(claims),
        )
    except Exception:
        log.exception("Error decoding access token")
    return None
