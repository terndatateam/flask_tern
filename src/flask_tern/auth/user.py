import warnings
from dataclasses import dataclass
from typing import Any

# TODO: (maybe) create a user factory method (per auth method?)
#       this factory method could be overridden in a custom settings.py to support different use cases.
#       the User object should be a data class so that it can easily be (de)serialised into/from the session.


# @dataclass(slots=True)
@dataclass
class User:
    """A class representing an authenticated user.

    `id` and `name` are required attributes.
    """

    #: Unique id identifying a user.
    id: str
    #: Users display name
    name: str
    #: Users email address
    email: str | None
    #: Flag whether users email address has been verified
    email_verified: bool
    #: List of roles within this app assigned to a user
    roles: list[str]
    #: List of scopes in OAuth token
    scopes: list[str]
    #: Dictionary of all claims taken from auth source (e.g. OAuth token)
    claims: dict

    def __init__(
        self,
        id: str,  # noqa: A002
        name: str,
        email: str | None = None,
        email_verified: bool = False,
        roles: list[str] | None = None,
        scopes: list[str] | None = None,
        claims: dict[str, Any] | None = None,
    ):
        """Create a user instance.

        Args:
            id: User id
            name: User display name
            email: User email (optional)
            email_verified: Has Users email been verified
            roles: list of roles assigned to user  (roles are simple strings)
            scopes: scopes taken form oauth token
            claims: Meant to hold all claims taken from oauth tokens. Can be used to store any
                    additional verified information about a user.
        """
        self.id = id
        self.name = name
        self.email = email
        self.email_verified = email_verified

        self.roles = roles or []
        self.scopes = scopes or []

        self.claims = claims or {}

    def has_role(self, role: str) -> bool:
        """Test if a user has given role.

        Args:
            role: role to test

        Returns:
            `True` if role is in `self.roles` otherwise `False`
        """
        return role in self.roles

    def has_roles(self, roles: list) -> bool:
        """Test if a user has all roles assigned.

        Args:
            roles: List of roles to test for.

        Returns:
            `True` if all roles are in `self.roles` otherwise `False`
        """
        return set(roles).issubset(set(self.roles))

    # convenience access to claims
    # TODO: check if there are any issues with dataclasses and __getattr__
    def __getattr__(self, name: str):
        """Convenience to access claims as attributes.

        Allows accessing claims just like any other attribute normal attributes still have precedence

        Args:
            name: attribute name

        Raises:
            AttributeError: if name is not in `self.claims`

        Returns:
            value of attribute / claim
        """
        if name == "claims":
            # catch the case where we have no attribute "claims"
            # otherwise next line will trigger endless recursive __getattr__ calls
            raise AttributeError()
        if name not in self.claims:
            raise AttributeError()
        return self.claims[name]

    def __getitem__(self, key):
        """Backwards compatible methods to allow dict based access to attributes.

        Supports access like `current_user['id']`

        Args:
            key: attribute to retrieve

        Returns:
            value of attribute
        """
        warnings.warn(
            "dict based access to user objects is deprecated",
            DeprecationWarning,
            stacklevel=2,
            source=True,
        )
        return getattr(self, key)

    def __contains__(self, item):
        """Backwards compatible methods to allow dict based access to attributes.

        Supports access like `'roles' in current_user`

        Args:
            item: attribute to check

        Returns:
            `True` if attribute is available
        """
        warnings.warn(
            "dict based containment test on user objects is deprecated",
            DeprecationWarning,
            stacklevel=2,
            source=True,
        )
        return hasattr(self, item)

    # support dict method .get(key, default)
    def get(self, key, default=None):
        """Backwards compatible methods to allow dict based access to attributes.

        Supports dict method like `.get(key, default)`

        Args:
            key: attribute to return
            default: default value if not attribute is not available

        Returns:
            Value of attribute
        """
        warnings.warn(
            "dict like .get on user objects in deprecated",
            DeprecationWarning,
            stacklevel=2,
            source=True,
        )
        if not hasattr(self, key):
            return default
        return getattr(self, key)
