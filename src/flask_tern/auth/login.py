# provide views for oauth2 / oidc dance
import logging
from dataclasses import asdict

from authlib.integrations.base_client.errors import OAuthError
from flask import Blueprint, current_app, redirect, request, session, url_for

from flask_tern.auth.user import User
from flask_tern.logging import audit_log, audit_success

from .oidc import get_oauth_userinfo, oauth

#: A blueprint providing views to handle browser session login
oidc_login = Blueprint("oidc_login", __name__)


def get_userinfo_from_session(auth_info):
    """Return User object based on auth_info.

    Args:
        auth_info: dict, A dictionary as returned by
                   :py:func:`flask_tern.auth.utils.get_authorization`. With keys
                   `type`: `bearer` and `token`: the encoded access token.

    Returns:
        'User' object with attributes set from auth_info
    """
    log = logging.getLogger(__name__)
    # TODO: return this, or do some fancy refresh cycle if we use access_token for auth
    #       flask session (id token), vs. oauth session (access_token)
    log.debug("REFRESH: %s", current_app.config["OIDC_USE_REFRESH_TOKEN"])
    if current_app.config["OIDC_USE_REFRESH_TOKEN"]:
        try:
            # get an OAuth2Session
            metadata = oauth.oidc.load_server_metadata()
            with oauth.oidc._get_oauth_client(**metadata) as client:
                # TODO: pass token=oauth.oidc.token into _get_oauth_client?
                # check our current is recent enough otherwise renew
                client.token = oauth.oidc.token
                # raises InvalidTokenError if there are any problems
                log.debug("Session token initiate token refresh: %s", oauth.oidc.token.__class__)
                client.token_auth.ensure_active_token()
                # refreshed token appears in oauth.oidc.token via on_update_token event

                # TODO: next release of OauthLib will have a method on client to do this:
                #          0.15.2 does not
                #       client.ensure_active_token(oauth.oidc.token) ...
                #          returns True or raises error?
                # TODO: should we reload userinfo access_token instead of relying on userinfo
                #       from initial auth?
                #       if changed ... also change authorize method
                #                      -> we get refreshed id and access token here
        except Exception:
            # something went wrong during token refresh, treat user as not logged in.
            log.exception("Session token refresh failed")
            return None

    session_user = session.get("oidc.user")
    if isinstance(session_user, dict):
        # convert dict to User object
        return User(**session_user)
    return session_user


# TODO: use this method everywhere (login, authorize and logout)
# TODO: make it possible to configure the route name 'root' or even a default return_url on
#       the application
def _save_return_url():
    """Store return url in session.

    Return url is used to redirect user back to where session based login process started.
    Return url can either be passed in as query parameter `return_url`, or is taken from
    HTTP Referer header.
    If none of these options returns a valid redirect url, it default to application `root` route.

    Returns:
        Return url as string.
    """
    # TODO: - validate return_url (should be on same domain / host?)
    #       - make this behaviour optional and allow a fixed return_url configured in app
    #         (also useful as fallback)
    return_url = request.args.get("return_url")
    if not return_url and request.referrer:
        return_url = request.referrer
    if return_url:
        session["return_url"] = return_url
    else:
        session.pop("return_url", None)
    return session.get("return_url", url_for("root", _external=True))


@oidc_login.route("/login")
def login():
    """Generate redirect url for oauth login.

    accepts url parameter `return_url` or uses Referrer header to remember return_url

    If there is no return_url set, it use the url for a route named ``root``, which needs
    to be configured on the Flask app.

    Returns:
        Redirect to authorize url.
    """
    _save_return_url()
    redirect_uri = url_for(".authorize", _external=True)
    return oauth.oidc.authorize_redirect(redirect_uri)


@oidc_login.route("/authorize")
@audit_log("login")
def authorize():
    """Parse and validate authorization response.

    On success return redirect user back to original URL and set session cookie

    If there is no return_url set, it use the url for a route named ``root``, which needs
    to be configured on the Flask app.

    Returns:
        Redirect to stored return url, or application `root` route.
    """
    # all tokens from auth response (id, access, refresh, and expiry infos)
    response = None
    try:
        # validation failed
        oauth.oidc.authorize_access_token()
        user = get_oauth_userinfo()

        # generate redirect url before clearing session
        redirect_url = session.pop("return_url", url_for("root", _external=True))

        if not user:
            # TODO: validation may have failed above ... e.g.
            #  InvalidClaimError - invalid claim "aud" ....
            #       but audit still says login success
            #
            # login failed: set audit success to false
            audit_success(False)
            # clear session, we don't know who the user is, better to clear all data
            session.clear()
        else:
            # store user as dict in session to avoid serialization problems
            session["oidc.user"] = asdict(user)
            if current_app.config["OIDC_USE_REFRESH_TOKEN"]:
                # store tokens for refresh
                session["oidc.tokens"] = oauth.oidc.token

            # check the token exists before logging
            if oauth.oidc.token is not None and "access_token" in oauth.oidc.token:
                logging.getLogger(__name__).debug(
                    "ACCESS_TOKEN: curl  -H 'Authorization: Bearer %s' localhost:5000",
                    oauth.oidc.token["access_token"],
                )

        response = redirect(redirect_url)
        # all done return to previous url
    except OAuthError as error:
        msg = error.error
        if msg is None:
            msg = "Access denied"

        response = redirect(session.pop("return_url", url_for("root", _external=True)))
        # set the auth_error cookie
        response.set_cookie("auth_error", msg)

    return response


@oidc_login.route("/logout")
def logout():
    """Clear logged in session.

    If there is no return_url set, it use the url for a route named ``root``, which needs to
    be configured on the Flask app.

    Returns:
        Redirect to return url.
    """
    # save return url
    return_url = _save_return_url()
    # clear session (end flask session)
    session.clear()
    # redirect to finish logout
    return redirect(return_url)
