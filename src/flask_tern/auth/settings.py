from collections.abc import Callable

from .apikey import get_userinfo_from_apikey
from .login import get_userinfo_from_session
from .oidc import get_userinfo_from_accesstoken
from .user import User

##########################################################
# Auth settings
##########################################################

#: A set of methods to extract user information based on authentication method.
#: E.g. `bearer` looks at HTTP `Authorization` header with bearer tokens, and `session`
#: uses sessions and cookies to store curretn login status. Other options can easily
#: be implemented. See :doc:`/addons/auth` for details
USERINFO_MAP: dict[str, Callable[[dict[str, str]], User | None]] = {
    "bearer": get_userinfo_from_accesstoken,
    "session": get_userinfo_from_session,
    "apikey-v1": get_userinfo_from_apikey,
}


# OIDC settings
#: The URL which serves an OIDC discovery document. This usually ends with
#: `/.well-known/openid-configuration`.
OIDC_DISCOVERY_URL = None
#: OIDC client id
OIDC_CLIENT_ID = None
#: OIDC client secret
OIDC_CLIENT_SECRET = None
#: OIDC scope. This needs to include `openid` to enable OpenID Connect protocol.
OIDC_SCOPE = "openid profile email"
#: If set to true, the application will store a refresh token in the session, and will keep
#: the access and id token valid (until refresh token or session timeout)
OIDC_USE_REFRESH_TOKEN = False

# APIKey settings
#: The API endpoint of the API Key service e.g. http://localhost:5000/api/v1.0.
#: The URL must not end with /.
APIKEY_SERVICE = None  # "http://localhost:5000/api/v1.0"
