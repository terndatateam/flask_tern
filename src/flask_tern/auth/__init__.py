from .decorators import require_login, require_roles, require_user
from .oidc import fetch_token, oauth, update_token
from .user import User
from .utils import current_user

__all__ = ["current_user", "init_app", "require_login", "require_roles", "require_user"]


def init_app(app):
    """This method is used to configure authentication via OIDC.

    Args:
        app: Flask app to configure

    It configures authlib Flask integration to verify tokens against the provided OIDC provider.

    Authlib's Flask integration relies on session storage. Please make sure to use a secure session
    implementation to avoid any issues.

    Typical usage would be::

        from flask_tern import auth

        def create_app():
            app = Flask(__name__)
            auth.init_app(app)
            return app

    """
    # oauth extension uses session to store temporary tokens and secrets.
    # make sure to use a good session storage backend.
    oauth.init_app(app=app, fetch_token=fetch_token, update_token=update_token)
    # register oauth client with name 'oidc'
    oauth.register(
        "oidc",
        # all options can also be read form app.config as OIDC_.....
        # OIDC_ is taken from name parameter here
        server_metadata_url=app.config["OIDC_DISCOVERY_URL"],
        client_id=app.config["OIDC_CLIENT_ID"],
        client_secret=app.config["OIDC_CLIENT_SECRET"],
        client_kwargs={"scope": app.config["OIDC_SCOPE"]},
    )
