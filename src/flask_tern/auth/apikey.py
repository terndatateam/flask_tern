from dataclasses import asdict

from flask import current_app

from flask_tern.auth import oauth
from flask_tern.auth.user import User
from flask_tern.cache import cache


def get_userinfo_from_apikey(auth_info: dict) -> User | None:
    """Method used by APP to verify ApiKey against api key service.

    Args:
        auth_info: dictionary with details about credentials provided in request

    Returns:
        `User` object filled with details about user

    ApiKey service returns user info for given api key.
    This auth method passes the given apikey on to apikey service for validation.
    """
    # log = logging.getLogger(__name__)
    # auth_info = {"type": "apikey-v1", "token": "base64" }

    # 1. get service token from keycloak
    APIKEY_SERVICE: str | None = current_app.config.get("APIKEY_SERVICE")
    if not APIKEY_SERVICE:
        return None
    # get api key from request
    apikey = auth_info["token"]
    cache_key = "auth.apikey.{}".format(apikey)
    user = cache.get(cache_key)
    # short circuit if apikey is still in cache
    if user:
        # re-create user from dict
        return User(**user)
    # call api key service to validate key and get user details
    oidc_md = oauth.oidc.load_server_metadata()
    # create OAuth2 Session
    client = oauth.oidc._get_oauth_client(**oidc_md)
    # fetch access token for service account
    client.fetch_token()
    response = client.post(APIKEY_SERVICE + "/apikeys/validate", json={"apikey": apikey})
    response.raise_for_status()
    token = response.json()
    # create user object
    user = User(
        id=token.get("sub"),
        name=token.get("name"),
        email=token.get("email"),
        email_verified=token.get("email_verified"),
        roles=(token.get("resource_access", {}).get(oauth.oidc.client_id, {}).get("roles", [])),
        scopes=[x.strip() for x in token.get("scope", "").split(" ") if x.strip()],
        # store all token claims in claims
        claims=token,
    )
    # cache user (as dict)
    cache.set(cache_key, asdict(user))
    return user
