from collections.abc import Callable, Iterable
from functools import wraps
from urllib.parse import urlencode

from flask import abort, redirect, request, url_for
from flask.typing import ResponseReturnValue

from .utils import current_user


# require authentication
def require_user(view_func: Callable) -> Callable[[], ResponseReturnValue]:
    """View decorator which enforces an authenticated user.

    If :py:data:`flask_tern.auth.current_user` is not set, then call :py:func:`flask:flask.abort`,
    which raises a 403 HTTP Exception.
    If we have a valid user, then execute the wrapped view method::

        @require_user
        def view():
            return "ok"

    # noqa: DAR201, DAR101
    """

    @wraps(view_func)
    def decorated(*args, **kwargs):
        if not current_user:
            abort(403)
        return view_func(*args, **kwargs)

    return decorated


def require_login(view_func: Callable) -> Callable[[], ResponseReturnValue]:
    """Redirect to login page if not logged in.

    If :py:data:`flask_tern.auth.current_user` is not set, then it will generate a redirect to a
    named endpoint ``oidc_login.login`` (as defined in blueprint `flask_tern.auth.login.oidc_login`).
    The current url is passed as url parameter `return_url` to the endpoint.
    After successful login, the user can be redirected back to the original url.
    If the user is not viewing from the web browser, we can return a 403 response.

    This wrapper can only be used with simple GET requests, as passing a post body on redirect may
    not work::

        @require_login
        def view():
            return "ok"

    # noqa: DAR101, DAR201
    """

    @wraps(view_func)
    def decorated(*args, **kwargs):
        if not current_user:
            if not (
                set(request.accept_mimetypes.values()) & {"text/html", "application/xhtml+html"}
            ):
                # if html/xhtml is not specifically accepted return 403
                abort(403)

            return redirect(
                # url_for is meant to set route params, so we should set query params ourselves
                # to get correct encoding.
                "".join(
                    (
                        url_for(
                            "oidc_login.login",
                            _external=True,
                        ),
                        "?",
                        urlencode((("return_url", request.url),)),
                    )
                ),
                302,
            )
        return view_func(*args, **kwargs)

    return decorated


# require authentication and a specific set of roles
def require_roles(roles: Iterable[str]) -> Callable[[Callable], Callable]:
    """Require an authenticated user with a specific set of roles.

    This wrapper ensures that there is an authenticated user, and all roles set in this decorator are present
    on the current users role attribute::

        @require_roles(["admin", "writer"])
        def view():
            return "ok"

    # noqa: DAR101, DAR201
    """
    req_roles = set(roles)

    def decorator(view_func: Callable) -> Callable[[], ResponseReturnValue]:
        @wraps(view_func)
        def decorated(*args, **kwargs):
            if not current_user:
                abort(403)
            if not req_roles.issubset(set(current_user.roles)):
                # all roles required
                abort(403)
            return view_func(*args, **kwargs)

        return decorated

    return decorator
