import base64
import logging

from flask import current_app, g, request, session
from werkzeug.local import LocalProxy

from flask_tern.auth.user import User


# a method to extract authentication information from the request.
# example implementation extracts 'Authorization` header and looks into session storage.
def get_authorization() -> dict | None:
    """Find and extract authentication information from current request.

    This method looks for an Authorization header or a valid session.
    It returns either None or a dictionary with keys `type` describing the authentication method
    and additional keys depending on the value of 'type'.

    Returns:
        Dictionary contains information about credentials provided.
    """
    # 1. check Authorization header
    auth_header = request.headers.get("Authorization")
    if auth_header:
        try:
            auth_type, auth_value = auth_header.split(" ", 1)
            auth_type = auth_type.lower()
        except (AttributeError, ValueError):
            # not a valid header
            return None
        try:
            if auth_type == "basic":
                username, password = base64.b64decode(auth_value).decode("utf-8").split(":", 1)
                if not password:
                    # if password is empty, then user name is API Key
                    # this works with e.g. curl https://<key>@<host>/path
                    return {"type": "apikey-v1", "token": username}
                elif not username or username == "apikey":
                    return {"type": "apikey-v1", "token": password}
                else:
                    # fall back to standard basic auth
                    return {"type": auth_type, "username": username, "password": password}
            elif auth_type == "bearer":
                return {"type": auth_type, "token": auth_value}

        except Exception:
            return None
    # 2. check X-API-Key header
    token = request.headers.get("X-API-Key")
    if token:
        return {"type": "apikey-v1", "token": token}
    # 3. check session
    if "oidc.user" in session:
        return {"type": "session"}
    return None


####################################################################################
# a LocalProxy for simple access to currently logged in user
#####################################################################################
def get_user() -> User | None:
    """Returns current authenticated user.

    This method is used to populate :py:data::`current_user`.
    It also `caches` the current user in :py:data::`flask.g`.

    Returns:
        Current authenticated User or None.
    """
    # check request headers and / or session for current user
    if not request:
        # not within a request
        return None
    if "current_user" in g:
        # we already have a a user
        return g.current_user
    # compute current user and cache result
    g.current_user = _get_user()
    return g.current_user


def _no_user(auth_info):
    # always return None as current user
    return None


def _get_user():
    log = logging.getLogger(__name__)
    # check auth header for user
    auth_info = get_authorization()
    g.auth_info = auth_info
    if not auth_info:
        # No auth_info (header or session) active
        g.auth_type = None
        return None
    g.auth_type = auth_info["type"]
    # We have some auth info (header or session)
    try:
        method = current_app.config["USERINFO_MAP"].get(auth_info.get("type"), _no_user)
        return method(auth_info)
    except Exception:
        log.exception("Authentication failed")
    return None


#: A LocalProxy which provides information about currently authenticated user.
current_user: User | LocalProxy[User | None] | None = LocalProxy(get_user)
