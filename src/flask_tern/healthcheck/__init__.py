import warnings
from collections.abc import Callable

from flask import Flask, current_app
from healthcheck import HealthCheck

from . import checks

HealthCheckFunction = Callable[[], tuple[bool, str]]


def add_check(app: Flask, checker: HealthCheckFunction):
    """Add a health check function to health check extension.

    Args:
        app: Flask app to register the function at
        checker : a callable executing the health check

    .. rubric:: Usage

    .. code:: python

        add_check(check_doi)
    """
    if not isinstance(app, Flask):
        warnings.warn(
            "healthcheck.add_check signature has changed. Please use 'add_check(app, checker)'",
            DeprecationWarning,
            stacklevel=2,
            source=True,
        )
        app, checker = checker, app
    app.extensions["healthcheck"].add_check(checker)


def init_app(app: Flask):
    """This method is used to configure py-healthcheck addon.

    It configures health check methods and adds a url rule to retrieve health check results.

    Args:
        app: Flask app to configure

    .. rubric:: Usage

    .. code:: python

        from flask_tern import healthcheck

        def create_app():
            app = Flask(__name__)
            healthcheck.init_app(app)
            return app

    """
    health = HealthCheck()
    app.extensions["healthcheck"] = health

    # add health check via
    health.add_section("version", app.config["VERSION"])

    # register bundled extension if configured
    if app.config.get("SQLALCHEMY_DATABASE_URI"):
        add_check(app, checks.check_db)
    if app.config.get("ELASTICSEARCH_URL"):
        add_check(app, checks.check_es)
    if app.config.get("APIKEY_SERVICE"):
        add_check(app, checks.check_apikey)
    if app.config.get("OIDC_DISCOVERY_URL"):
        add_check(app, checks.check_keycloak)
    if app.config.get("DOI_URL"):
        add_check(app, checks.check_doi)

    # TODO: could check app.extension to figure which health checks to activate?

    # TODO: add access protection rules
    # FIXME: this should be part of blueprint? or some other thing that defines the /api part?
    #        (just url_prefix or so maybe?)
    app.add_url_rule("/api/_health", "healthcheck", view_func=lambda: health.run())
    # app.add_url_rule("/_env", "environment", view_func=lambda: envdump.run())
