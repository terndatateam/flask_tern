from urllib.parse import urljoin

import requests
from flask import current_app
from sqlalchemy.sql import text


# add your own check function to the health check
def check_db():
    """A health check method testing database connectivity."""
    from flask_tern.db import db

    try:
        db.session.execute(text("SELECT 1"))
    except Exception as e:
        return False, "Database failed: {}".format(str(e))
    return True, "Database_ok"


def check_es():
    """A health check method testing elasticsearch connectivity."""
    from elasticsearch_dsl import connections

    try:
        es = connections.get_connection()
        _ = es.search(body={"size": 0})
    except Exception as e:
        return False, "Elasticsearch failed: {}".format(str(e))
    return True, "Elasticsearch_ok"


def check_doi():
    """A health check method testing DOI service connectivity."""
    import xmltodict

    try:
        xml = "test"
        headers = {"Accept": "application/xml"}
        payload = {"xml": xml}
        package_minting_url = "{}&url=status.url.check".format(current_app.config["DOI_URL"])
        result = requests.post(package_minting_url, data=payload, headers=headers, timeout=30)
        if result.status_code != 200:
            raise Exception("{} - {}".format(str(result), result.content))
        result_xml = xmltodict.parse(result.text)
        if "The DOI service is currently unavailable." in result_xml["response"]["message"]:
            raise Exception(result_xml["response"]["message"])
    except Exception as e:
        return False, "DOI failed: {}".format(str(e))
    return True, "DOI_ok"


def check_apikey():
    """A health check method testing apikey_api connectivity."""
    apikey_service = current_app.config.get("APIKEY_SERVICE")
    if not apikey_service:
        return False, "API Key service not configured."
    try:
        response = requests.get(urljoin(apikey_service, "_health"), timeout=30)
        response.raise_for_status()
    except Exception as e:
        return False, "API Key service failed: {}".format(str(e))
    return True, "API Key service ok"


def check_keycloak():
    """A health check method testing elasticsearch connectivity."""
    try:
        url = current_app.config["OIDC_DISCOVERY_URL"]
        response = requests.get(url, timeout=30)
        response.raise_for_status()
        # TODO:  do I need to validate md ?
        #        is there a health endpoint to keycloak?
    except Exception as e:
        return False, "Keycloak failed: {}".format(str(e))
    return True, "Keycloak ok"
