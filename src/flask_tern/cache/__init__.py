from flask_caching import Cache

# TODO: make caching configurable via env vars

#: global :py:class:`flaskcache:flask_caching.Cache` instance
cache = Cache()


def init_app(app):
    """Configure :doc:`flaskcache:index` extension."""
    #################################################################
    # Configure caching
    #################################################################
    cache.init_app(app)
