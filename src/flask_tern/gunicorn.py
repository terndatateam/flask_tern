# A Gunicorn config file used to set up logging
# access_log_format: https://docs.gunicorn.org/en/stable/settings.html#access-log-format

logconfig_dict = {
    "version": 1,
    "incremental": False,
    "disable_existing_loggers": False,
    "formatters": {
        "json": {
            "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
            # TODO: normal format
            "format": "%(name)-12s — %(levelname)-8s — %(message)s",
            # TODO: our audit format
            # "format": "%(name)-12s — %(levelname)-8s",
            "timestamp": True,
        },
        "default": {
            "format": "%(asctime)s [%(process)d] [%(levelname)s] [%(name)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        # TODO: would need a custom Logger instance or patch gunicorm.glogging.Logger.access_logger
        #       somehow (e.g. use a LogAdapter there?)
        "access": {"format": "%(message)s"},
    },
    "handlers": {
        "stdout": {
            "class": "logging.StreamHandler",
            # TODO: should this be json as well? ... see above access format
            "formatter": "access",
            "stream": "ext://sys.stdout",
            "level": "NOTSET",
        },
        "stderr": {
            "class": "logging.StreamHandler",
            "formatter": "json",
            "stream": "ext://sys.stderr",
            "level": "NOTSET",
        },
    },
    "loggers": {
        # access logs go to stdout
        "gunicorn.access": {"propagate": 0, "handlers": ["stdout"]},
        # all other logs to stderr
        "gunicorn.error": {"propagate": 0, "handlers": ["stderr"]},
    },
    # all logs to stderr as json
    "root": {"level": "WARNING", "handlers": ["stderr"]},
}
