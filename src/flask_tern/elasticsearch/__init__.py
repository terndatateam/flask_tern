import flask
from elasticsearch_dsl import connections


def init_app(app: flask.Flask):
    """Configure :doc:`elasticsearch_dsl <esdsl:index>` connections pool along a Flask application.

    Main purpose is to have consistent place on how to set up extension and third party
    libraries along a Flask application, and also to unify configuration loading.

    This method just uses `app.config` to create a default :doc:`elasticsearch_dsl <esdsl:index>`
    connection.

    Args:
        app: Flask app to configure.

    .. rubric:: Usage

    .. code:: python

        from flask_tern import elasticsearch

        def create_app():
            app = Flask(__name__)
            elasticsearch.init_app(app)
            return app

    """
    # Let this fail if ELASTICSEARCH_URL is not set.
    # app.config.setdefault("ELASTICSEARCH_URL", "http://localhost:9200/")
    # initialise ES connection
    connections.create_connection(
        hosts=[app.config.get("ELASTICSEARCH_URL")],
        verify_certs=app.config.get("ELASTICSEARCH_VERIFY_CERTS"),
        ssl_show_warn=app.config.get("ELASTICSEARCH_SSL_SHOW_WARN"),
        request_timeout=app.config.get("ELASTICSEARCH_REQUEST_TIMEOUT", 30),
    )
