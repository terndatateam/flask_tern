##########################################################
# Elasticsearch settings
##########################################################

#: Elasticsearch URL. This url should include credentials and index name if required.
ELASTICSEARCH_URL = None  # "http://localhost:9200"
#: Enable / disable certificate validation in case fol `https` connections to Elasticsearch.
ELASTICSEARCH_VERIFY_CERTS = True
#: Enable / disable untrusted SSL certificate warnings in case certificate verification das been disabled.
ELASTICSEARCH_SSL_SHOW_WARN = True
#: Set Request Timeout for Elasticsearch client
ELASTICSEARCH_REQUEST_TIMEOUT = 30
