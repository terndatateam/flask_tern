"""Define deault settings for all extensions.

Also define a function to load all settings as usual.
"""

import datetime
import os

##########################################################
# Session and Cookie settings
##########################################################

#: value to use to sign / encrypt session cookie. If unset, the app will auto generate a value.
#: This value needs to be configured if load balancing across multiple processes is used.
SECRET_KEY = os.urandom(16)

#: Session backend type. The default is to use cookies as session storage.
SESSION_TYPE = None
#: If `SESSION_TYPE = "filesystem"`, this option specifies the storage location for session data.
#: as default it uses the `flask_session_store` folder within current working directory.
SESSION_FILE_DIR = os.path.join(os.getcwd(), "flask_session_store")
#: If `SESSION_TYPE = "filesystem"`, this option sets the maximum number of items the session
#: stores before it starts deleting some.
#: default is 500.
SESSION_FILE_THRESHOLD = 500
#: If 'SESSION_TYPE = "filesystem"` this option sets the file mode for the session files.
#: as default it uses `0o600`. If given as string it will be parsed as integer or
#: if prefixed with `0o` as octal number.
SESSION_FILE_MODE = 0o600
#: If `SESSION_TYPE = "redis"`, this option should be a configured :py:class:`redis:redis.Redis`
#: instance. If loaded from environment variables, it can be specified as connection url as
#: described in :py:func:`redis:redis.from_url`
SESSION_REDIS = None

SESSION_COOKIE_SAMESITE = "Lax"
PERMANENT_SESSION_LIFETIME = datetime.timedelta(days=1)
MAX_COOKIE_SIZE = 8192

##########################################################
# SQLAlchemy settings
##########################################################

#: It is required to configure this setting if SQLAlchemy is being used.
SQLALCHEMY_DATABASE_URI = None
#: avoid SQLAlchemy warning
SQLALCHEMY_TRACK_MODIFICATIONS = False
#: default SQLAlchemy connection pool configuration (can only be overriden with a settings file)
SQLALCHEMY_ENGINE_OPTIONS = {
    "pool_size": 5,
    "pool_recycle": 300,
    "pool_pre_ping": True,
}

#: deault DOI URL
DOI_URL = None  # "https://doi.example.com/test/index.php?r=api/create&user_id=test-user@example.com&app_id=app-id"  # noqa

#: default CACHE_TYPE used for Flask-Caching extension.
CACHE_TYPE = "flask_caching.backends.nullcache.NullCache"
